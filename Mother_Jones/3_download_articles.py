import os
import time

with open("cache/clean_links.txt", "r") as file:
    article_links = file.readlines()
    pass

print(f"{len(article_links)} possible articles to download, do you want to continue?")
agreement = input("Enter Y to continue and N to stop: ")
if agreement == "Y":
    pass
if agreement == "N":
    exit()

# trying the scraper on the links
# article extraction
for indx, link in enumerate(article_links):
    print(f"link {link}")
    print(f"working on the link number {indx}")
    os.system(f"python scraper.py -l {link}")
    time.sleep(0.1)
    pass
