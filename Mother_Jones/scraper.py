import argparse
import random
import shutil
import requests
import shortuuid
from bs4 import BeautifulSoup
import os
from fake_useragent import UserAgent
import json
from datetime import datetime, timedelta
from dateutil import parser, tz

# update x.5
# transforming the scraper into a script so the link could be called from the main script
arg_parser = argparse.ArgumentParser(
    prog="help",
    description="This script takes one article link from abc news and scrapes the article \n"
                " and saves it as a folder with images and json file",
    epilog="If there are problems contact amirreza.alise@gmail.com"
)
arg_parser.add_argument("-l", "--Link", required=True,
                    help="The link to the article \n"
                         "eg. https://abcnews.go.com/US/110-million-alert-severe-dangerous-weather-holiday-weekend/story?id=100573102")
args = arg_parser.parse_args()

path = args.Link
#path = str(input("Enter the URL of the article: "))
# eg. https://abcnews.go.com/US/110-million-alert-severe-dangerous-weather-holiday-weekend/story?id=100573102


## update x.6
# building proxy list to avoid getting blocked
def LoadUpProxies():
    try:
        proxies = []
        url = 'https://sslproxies.org/'
        ua = UserAgent()
        header = {"User-Agent": ua.random}
        response = requests.get(url, headers=header)
        soup = BeautifulSoup(response.content, 'html.parser')
        soup2 = soup.find("div", class_="table-responsive fpl-list")
        soup3 = soup2.find("table", class_="table table-striped table-bordered").find("tbody").find_all("tr")[1:20]
        for i in soup3:
            try:
                ip = i.find_all('td')[0].string
                port = i.find_all('td')[1].string
                proxy = {'ip': ip, 'port': port}
                proxies.append(proxy)
            except:
                print('')
    except:
        proxies = [{'ip': '190.61.88.147', 'port': '8080'}, {'ip': '129.159.112.251', 'port': '3128'},
                   {'ip': '186.121.235.66', 'port': '8080'}, {'ip': '116.111.222.9', 'port': '23775'},
                   {'ip': '167.172.80.53', 'port': '3128'}, {'ip': '64.225.4.63', 'port': '9990'},
                   {'ip': '173.176.14.246', 'port': '3128'}, {'ip': '178.33.3.163', 'port': '8080'},
                   {'ip': '20.44.206.138', 'port': '80'}, {'ip': '129.153.157.63', 'port': '3128'},
                   {'ip': '158.160.56.149', 'port': '8080'}, {'ip': '186.121.235.222', 'port': '8080'},
                   {'ip': '3.141.13.89', 'port': '80'}, {'ip': '116.111.217.187', 'port': '23775'},
                   {'ip': '20.210.26.214', 'port': '3333'}, {'ip': '116.111.218.37', 'port': '23775'},
                   {'ip': '64.225.4.29', 'port': '9865'}, {'ip': '64.225.8.135', 'port': '9995'},
                   {'ip': '8.219.97.248', 'port': '80'}]
    return proxies


ua = UserAgent()
headers = {"User-Agent": ua.random}
try:
    # update x.6
    proxies = LoadUpProxies()
    proxy_index = random.randint(0, len(proxies) - 1)
    proxy_ip = proxies[proxy_index]['ip']
    proxy_port = proxies[proxy_index]['port']
    ###
    doc = requests.get(args.Link, headers=headers, proxies={"http": f"http://{proxy_ip}:{proxy_port}"}) # end of update x.6
    doc.encoding = "utf8"
    doc = BeautifulSoup(doc.text, "html.parser")
    pass

except:
    print("The URL or file name is invalid.")
    pass

# headline
try:
    headline = doc.find("h1", class_="entry-title").string
    pass
except:
    headline = ""
    pass


# subheading
try:
    subheading = doc.find("h2", class_="dek").string
    pass
except:
    try:
        subheading = doc.find("h3", class_="dek").string
        pass
    except:
        subheading = ""
        pass

# author
def author_finder(doc):
    author = []
    try:
        spans = doc.find_all("li", class_="author-bio group vcard")
        spans = [s.find("div", class_="author-data").find("h3").find("span").find("a") for s in spans]
        authors = [a for a in spans]
        if len(authors) == 1:
            author = [authors[0].string]
        else:
            author = [a.string for a in authors]

            try:
                author = [doc.find("li", class_="author-bio group vcard").find("div", class_="author-data").find("h3").\
                          find("span").find("a").string]
                pass

            except:
                try:
                    author = [doc.find("p", class_="byline-dateline").find_all("span")[0].find("a").string]
                    pass

                except:
                    author = [doc.find("p", class_="byline-dateline").find_all("span")[0].find("span").string]
                    pass
    except:
        pass

    return author


author = author_finder(doc)

# date
def date_finder(doc):
    try:
        # this will execute if the article was published within the last 24 hours eg. 2 hours ago
        date = doc.find("span", class_="dateline").find("span").string

        # then we need to convert the date into a format that we can use eg. May 26, 2023
        # we can use the datetime module to do this, we have to subtract the number of hours from the current time
        # we can use the split function to get the number of hours from the string
        hours = int(date.split(" ")[0])
        # we can then use the timedelta function to subtract the number of hours from the current time and save in utc timezone
        date1 = datetime.now(tz=tz.tzutc()) - timedelta(hours=hours)
        # we can then use the strftime function to convert the date into a string
        #date_utc = date1.astimezone(tz.tzutc()).isoformat()

        date_iso8601 = date1.isoformat().split("+")[0] + "Z"
        pass

    except:
        try:
            # normal case
            date = doc.find("span", class_="dateline").string
            date_obj = parser.parse(date)
            date_utc = date_obj.astimezone(tz.tzutc()).isoformat()
            date_iso8601 = date_utc.split("+")[0] + "Z"
            pass
        except:
            date_iso8601 = ""
            pass

    return date_iso8601


date_iso8601 = date_finder(doc)

# article body
try:
    article_body = doc.find("article", class_="entry-content").find_all("p")
    text = "".join([p.text for p in article_body])
    # taking the first 500 characters and adding a [...] at the end
    #text = text[:500]+"[...]"
    pass

except:
    text = ""
    pass

id = shortuuid.uuid()
#path_name = slugify(headline, allow_unicode=True)
dest_path = f"{id}/"
if headline != "404":
    if not os.path.exists(dest_path):
        os.makedirs(dest_path)
    else:
        print("The article with this id already exists.")
        pass
    pass
else:
    print("error 404")


def image_finder_single(doc):
    valid_imgs = []
    pictures = doc.find_all("img", class_="skip-lazy wp-post-image")
    for picture in pictures:
        img_src = picture["src"]
        img_name = img_src.split("/")[-1].split("?")[0]

        try:
            img_data = requests.get(img_src, headers=headers).content
            print(f"Downloading {img_name}  ...")

            # update x.x.2 fixing JPG formatting
            existing_format = img_name.split(".")[-1]
            if existing_format != "jpg":
                img_name = img_name.replace(f".{existing_format}", ".jpg")
                pass

            # we download the image with path img_src into a dist_path and adding .png extension
            with open(f"{dest_path}{img_name}", "wb") as file:
                file.write(img_data)

            try:
                img_desc = picture["alt"]
                if img_desc is None or img_desc == "":
                    fig_cap = picture.find_parent("div").find("p").find("span", class_="media-credit").string
                    img_desc = "credits: " + fig_cap

                image = {"filename": img_name, "description": img_desc}
            except:
                img_desc = None
                image = {"filename": img_name}
                pass

            valid_imgs.append(image)
            break
        except:
            pass

    return valid_imgs


def image_finder_photo_articles(doc):
    valid_imgs = []
    header_image = doc.find("div", class_="header-image")
    pictures = doc.find_all("figure")
    # adding header image to first of the pictures list to be iterated
    pictures.insert(0, header_image)
    for picture in pictures:
        img = picture.find("img")
        img_src = img["src"]
        img_name = img_src.split("/")[-1].split("?")[0]

        try:
            img_data = requests.get(img_src, headers=headers).content
            print(f"Downloading {img_name}  ...")
            # we download the image with path img_src into a dist_path and adding .png extension

            # update x.x.2 fixing JPG formatting
            existing_format = img_name.split(".")[-1]
            if existing_format != "jpg":
                img_name = img_name.replace(f".{existing_format}", ".jpg")
                pass

            with open(f"{dest_path}{img_name}", "wb") as file:
                file.write(img_data)

            try:
                img_desc = None
                if picture == header_image:
                    print("extracting header image caption")
                    # it means that the image is the header image
                    fig_cap = picture.find("p").find("span").string
                    img_desc = fig_cap

                if img_desc is None or img_desc == "":
                    fig_cap = picture.find("figcaption").find_all("p")[0].find("span").string
                    fig_cred = picture.find("figcaption").find_all("p")[1].find("span").string
                    img_desc = fig_cap + "credit: " + fig_cred


                image = {"filename": img_name, "description": img_desc}
            except:
                img_desc = None
                image = {"filename": img_name}
                pass

            valid_imgs.append(image)

        except:
            pass

    return valid_imgs


try:
    valid_imgs = image_finder_photo_articles(doc)
except:
    valid_imgs = image_finder_single(doc)
    pass


# tags
try:
    tags = doc.find("div", class_="article-tags").find_all("a")
    pass
except:
    tags = []
    pass

# update x.4
if len(valid_imgs) == 0:
    # stop the program and delete the dest_path folder if there is no image
    print(f"There are no images in this article. Deleting folder {dest_path} ...")
    shutil.rmtree(dest_path)
    exit()

JSON_dict = {
    "url": path,
    "heading": headline,
    "subheading": subheading,
    "text": text,
    "author": author,
    "publish_date": date_iso8601,
    "tags": tags,
    "img_desc": valid_imgs,
    "id": id
}

with open(f"{dest_path}article.json", "w") as outfile:
    json.dump(JSON_dict, outfile, indent=4)

