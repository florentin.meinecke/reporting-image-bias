import os
import numpy as np
from fix_encoding import fix_encoding

fix_encoding("cache/links.txt")

def load_links(file):
    with open(file, 'r') as f:
        links = f.readlines()

    return links

# update x.7
prev_links = []
new_links = []
# load previous clean_links if exists
if os.path.exists("cache/clean_links.txt"):
    prev_links = load_links("cache/clean_links.txt")

if os.path.exists("cache/links.txt"):
    new_links = load_links("cache/links.txt")

print("number of new links:", len(new_links))
print("number of old links:", len(prev_links))

#cleaning the links file
with open("cache/clean_links.txt", "w+", encoding="utf-8") as f:
    clean_links = []
    links = new_links
    temp = np.append(links, prev_links)
    temp2 = np.unique(temp)

    for link in temp2:
        if link == "\n":
            continue
        if type(link) != str:
            link = str(link)

        if "?url=" in link:
            link = link.split("?url=")[1]
        if "?u=" in link:
            link = link.split("?u=")[1]


        # continue if link not starts with http://www.politico.com or https://www.politico.com
        if "theintercept.com" not in link:
            continue

        # if theintercept.com is not at the beginning of the link
        if "theintercept.com" not in link[0:40]:
            continue

        if "join.theintercept.com" in link:
            continue

        if link[0:7] != "http://":
            if link[0:8] != "https://":
                continue

        clean_links.append(link)
        f.write(link)

print("new total unique links:", len(clean_links))

"""
# delete the links file
if os.path.exists("cache/links.txt"):
    os.remove("cache/links.txt")
"""
