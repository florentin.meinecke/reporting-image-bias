import os
import json
import shutil

# iterating over the folders(articles) and reading the json file
# saving the folder ID and the link in the json file in a dictionary
# if the link is already in the dictionary, delete the folder


def remove_duplicates(path):

    json_dict = {}
    try:
        folders = os.listdir(path)
        print(f'{len(folders)} folders found.')
    except FileNotFoundError:
        print(f'folder {path} could not be found.')
        return False
    for folder in folders:
        print(f'checking folder {folder}')
        if '.py' in folder:
            continue
        if folder == 'cache':
            continue

        try:
            with open(os.path.join(path, folder, 'article.json'), 'r') as f:
                json_file = json.load(f)
        except FileNotFoundError:
            print(f'article.json for {folder} folder not found.')
            shutil.rmtree(os.path.join(path, folder))
            continue

        if json_file["url"] in json_dict:
            print(f'link {json_file["url"]} is already in dictionary.')
            print(f'folder {folder} will be deleted.')
            shutil.rmtree(os.path.join(path, folder))
        else:
            json_dict[json_file["url"]] = json_file["id"]
    return


remove_duplicates('../the_intercept')
