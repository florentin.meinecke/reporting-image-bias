import codecs
import glob
import re
import chardet

def fix_encoding(path):
    ALL_FILES = glob.glob(path)

    def kira_encoding_function():
        """Check encoding and convert to UTF-8, if encoding no UTF-8."""
        for filename in ALL_FILES:

            # Not 100% accuracy:
            # https://stackoverflow.com/a/436299/5951529
            # Check:
            # https://chardet.readthedocs.io/en/latest/usage.html#example-using-the-detect-function
            # https://stackoverflow.com/a/37531241/5951529
            with open(filename, 'rb') as opened_file:
                bytes_file = opened_file.read()
                #bytes_file = bytes_file.replace(b'\x9d', b'\x20')
                #bytes_file = bytes_file.replace(b'\x98', b'\x20')
                #bytes_file = bytes_file.replace(b'\x90', b'\x20')
                # removing undefined character from the bytes_file
                bytes_file = re.sub(b'\x9d', b'\x20', bytes_file)
                chardet_data = chardet.detect(bytes_file)
                fileencoding = (chardet_data['encoding'])
                print('file encoding', fileencoding)

                if fileencoding in ['utf-8', 'ascii']:
                    print(filename + ' in UTF-8 encoding')
                else:
                    # Convert file to UTF-8:
                    # https://stackoverflow.com/q/19932116/5951529
                    cyrillic_file = bytes_file.decode('cp1251')
                    with codecs.open(filename, 'w', 'utf-8') as converted_file:
                        converted_file.write(cyrillic_file)
                    print(filename +
                          ' in ' +
                          fileencoding +
                          ' encoding automatically converted to UTF-8')


    kira_encoding_function()

    return