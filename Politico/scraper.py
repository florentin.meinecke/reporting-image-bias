import argparse
import random
import requests
from bs4 import BeautifulSoup
import os
from fake_useragent import UserAgent
import json
import shortuuid
from dateutil import parser, tz
import shutil
import re


# update x.5
# transforming the scraper into a script so the link could be called from the main script
arg_parser = argparse.ArgumentParser(
    prog="help",
    description="This script takes one article link from abc news and scrapes the article \n"
                " and saves it as a folder with images and json file",
    epilog="If there are problems contact amirreza.alise@gmail.com"
)
arg_parser.add_argument("-l", "--Link", required=True,
                    help="The link to the article \n"
                         "eg. https://www.politico.com/news/2023/09/20/zelenskyy-us-military-pentagon-biden-00117199")
args = arg_parser.parse_args()

path = args.Link

#path = "https://www.politico.com/news/2023/09/22/menendez-allegations-gold-cash-car-00117646"
# eg. https://www.politico.com/news/2023/09/20/zelenskyy-us-military-pentagon-biden-00117199


## update x.6
# building proxy list to avoid getting blocked
def LoadUpProxies():
    try:
        proxies = []
        url = 'https://sslproxies.org/'
        ua = UserAgent()
        header = {"User-Agent": ua.random}
        response = requests.get(url, headers=header)
        soup = BeautifulSoup(response.content, 'html.parser')
        soup2 = soup.find("div", class_="table-responsive fpl-list")
        soup3 = soup2.find("table", class_="table table-striped table-bordered").find("tbody").find_all("tr")[1:20]
        for i in soup3:
            try:
                ip = i.find_all('td')[0].string
                port = i.find_all('td')[1].string
                proxy = {'ip': ip, 'port': port}
                proxies.append(proxy)
            except:
                print('')
    except:
        proxies = [{'ip': '190.61.88.147', 'port': '8080'}, {'ip': '129.159.112.251', 'port': '3128'},
                   {'ip': '186.121.235.66', 'port': '8080'}, {'ip': '116.111.222.9', 'port': '23775'},
                   {'ip': '167.172.80.53', 'port': '3128'}, {'ip': '64.225.4.63', 'port': '9990'},
                   {'ip': '173.176.14.246', 'port': '3128'}, {'ip': '178.33.3.163', 'port': '8080'},
                   {'ip': '20.44.206.138', 'port': '80'}, {'ip': '129.153.157.63', 'port': '3128'},
                   {'ip': '158.160.56.149', 'port': '8080'}, {'ip': '186.121.235.222', 'port': '8080'},
                   {'ip': '3.141.13.89', 'port': '80'}, {'ip': '116.111.217.187', 'port': '23775'},
                   {'ip': '20.210.26.214', 'port': '3333'}, {'ip': '116.111.218.37', 'port': '23775'},
                   {'ip': '64.225.4.29', 'port': '9865'}, {'ip': '64.225.8.135', 'port': '9995'},
                   {'ip': '8.219.97.248', 'port': '80'}]
    return proxies


ua = UserAgent()
headers = {"User-Agent": ua.random}
try:
    # update x.6
    proxies = LoadUpProxies()
    proxy_index = random.randint(0, len(proxies) - 1)
    proxy_ip = proxies[proxy_index]['ip']
    proxy_port = proxies[proxy_index]['port']
    ###
    doc = requests.get(path, headers=headers, proxies={"http": f"http://{proxy_ip}:{proxy_port}"}) # end of update x.6
    doc.encoding = "utf8"
    doc = BeautifulSoup(doc.text, "html.parser")
    pass

except:
    print("The URL or file name is invalid.")
    pass


def find_headline(doc):
    try:
        headline = doc.find("h2", class_="headline").string

    except:
        try:
            headline = "" # pseudo-code placeholder

        except:
            headline = ""

    return headline


# headline
headline = find_headline(doc)

id = shortuuid.uuid()
# path_name = slugify(headline, allow_unicode=True)
dest_path = f"{id}/"
if headline != "404":
    if not os.path.exists(dest_path):
        os.makedirs(dest_path)
    else:
        print("The article with this id already exists.")
        pass
    pass
else:
    print("error 404")


# subheading
# CNN does not have subheadings this is a placeholder
try:
    subheading = doc.find("p", class_="dek").string
    pass
except:
    try:
        subheading = ""
    except:
        subheading = ""
    pass


# author
def author_finder(doc):
    authors = []
    try: #eg <a href="https://www.politico.com/staff/lara-seligman" target="_top">Lara Seligman</a>
        # find a line that has /staff/ in it
        pattern_line = re.compile(r'/staff/.*</a>')
        lines = re.findall(pattern_line, str(doc))
        # find the line that has the author name in it
        pattern_author = re.compile(r'(?<=>).*?(?=</)')
        for line in lines:
            author = re.findall(pattern_author, line)
            authors.extend(author)


    except:
        pass

    return authors


def author_finder2(doc):
    try:
        authors = doc.findall("span", class_="vcard")
        authors = [a.find("a").string for a in authors]

        return authors
    except:
        return []

author = author_finder(doc)
if len(author) == 0:
    author = author_finder2(doc)

def date_finder(doc):
    try:
        try:
            date = doc.find("time")["datetime"]
            # desired output: 2023-06-28T16:03:00.000Z

        except:
            date = None
            # output:
            '''
            5:39 a.m. ET, July 17, 2023
            '''

        if date == None:
            date_iso8601 = ""
            return date_iso8601

        date = date.strip() # 2:45 PM EDT, Fri July 14, 2023
        datetime_object = parser.parse(date)  # 2023-07-11 00:00:00+03:00
        #print("date_obj", datetime_object)
        #datetime_object = datetime_object.replace(tzinfo=None) + timedelta(hours=6)
        date_utc = datetime_object.astimezone(tz.gettz()).isoformat()  # 2023-07-11T00:00:00+03:00
        date_iso8601 = date_utc.split("+")[0] + "Z"  # 2023-07-11T03:00:00Z
        pass
    except:
        date_iso8601 = ""
        pass

    return date_iso8601


date_iso8601 = date_finder(doc)


# article body
def text_finder(doc):
    try:
        # if followed by <p class="story-text__paragraph  "> and ends with </p>
        pattern = re.compile(r'(?<=<p class="story-text__paragraph">).*?(?=</p>)')
        paragraphs = re.findall(pattern, str(doc))
        #paragraphs = [p for s in sections for p in s.find_all("p")]
        text = "\n".join([p for p in paragraphs])

    except:
        text = ""
        pass

    return text


text = text_finder(doc)


def image_finder(doc):
    valid_imgs = []

    try:
        header_image = None # pseudo-code placeholder
    except:
        header_image = None
        pass

    try:
        article = None # pseudo-code placeholder
        pictures = []


        # pseudo-code placeholder
        try:
            # JS path = document.querySelector("#main > div > section:nth-child(1) > div > div.container__column.container__column--story.summary-middle > div:nth-child(1) > div:nth-child(2) > section > div.media-item__image > div > figure > div > picture")
            divs = doc.find_all("figure", class_="art")
            pics_wraper = [d.find("div", class_="fig-graphic") for d in divs]

            for p in pics_wraper:
                if p not in pictures:
                    pictures.append(p)

        except:
            pass

        try:
            divs = doc.find_all("figure", class_="story-photo")
            pics_wraper = [d.find("div", class_="story-photo__image").find("span") for d in divs]

            for p in pics_wraper:
                if p not in pictures:
                    pictures.append(p)

        except:
            pass

    except:
        pictures = []
        pass

    # adding header image to first of the pictures list to be iterated
    if header_image is not None:
        pictures.insert(0, header_image)

    print(f"{len(pictures)} image-wrappers plus header found.")
    for picture_wraper in pictures:
        if picture_wraper == header_image:
            picture = None
        else:
            try:
                picture = picture_wraper.find("picture")
                pass
            except:
                try:
                    picture = None
                    pass
                except:
                    try:
                        picture = None
                        pass
                    except:
                        try:
                            picture = None
                            pass
                        except:
                            continue


        img = picture.find("img")
        img_src = img["src"]
        img_name = img_src.split("/")[-1].split("%2")[-1]

        try:
            img_data = requests.get(img_src, headers=headers).content
            print(f"Downloading {img_name}  ...")
            # we download the image with path img_src into a dist_path and adding .png extension
            # we have to convert images to jpg ,so we replace the existing extension with .png if there is one
            existing_format = img_name.split(".")[-1]
            if existing_format != "jpg":
                img_name = img_name.replace(f".{existing_format}", ".jpg")
                pass

            with open(f"{dest_path}{img_name}", "wb") as file:
                file.write(img_data)
                pass

            try:
                img_desc = None
                try:
                    img_title = picture["title"]
                    if img_title is not None:
                        img_desc = img_title
                        pass
                except:
                    pass
                try:
                    img_desc = img["alt"]
                except:
                    pass

                if img_desc is not None:
                    image = {"filename": img_name, "description": img_desc}
                else:
                    image = {"filename": img_name}
                pass
            except:
                img_desc = None
                image = {"filename": img_name}
                pass

            valid_imgs.append(image)

            pass
        except:
            pass

    return valid_imgs


valid_imgs = image_finder(doc)


# tags
# Politics has no tags
tags = []

# update x.4
if len(valid_imgs) == 0:
    # stop the program and delete the dest_path folder if there is no image
    print(f"There are no images in thi article. Deleting folder {dest_path} ...")
    shutil.rmtree(dest_path)
    exit()


JSON_dict = {
    "url": path,
    "heading": headline,
    "subheading": subheading,
    "text": text,
    "author": author,
    "publish_date": date_iso8601,
    "tags": tags,
    "img_desc": valid_imgs,
    "id": id
}

with open(f"{dest_path}article.json", "w") as outfile:
    json.dump(JSON_dict, outfile, indent=4)
