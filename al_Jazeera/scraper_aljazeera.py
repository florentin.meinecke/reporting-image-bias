import requests
from bs4 import BeautifulSoup
from dateutil import parser
from dateutil import tz
import shortuuid
import json
from fake_useragent import UserAgent
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import os
from datetime import datetime
from urllib.parse import urljoin  # Import urljoin function


# Function to scroll to the "Show more" button using JavaScript
def scroll_to_show_more(driver):
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

# Define the starter link
starter_link = 'https://www.aljazeera.com/'

# Launch a web browser
driver = webdriver.Chrome()  # other browsers too

# Load the starter link
driver.get(starter_link)

# List of tags and their corresponding categories
tags_and_categories = [
    # ("live", "Live"),
    ("news/", "News"),
    ("middle-east/", "Middle East"),
    ("africa/", "Africa"),
    ("asia/", "Asia"),
    ("us-canada/", "US & Canada"),
    ("latin-america/", "Latin America"),
    ("europe/", "Europe"),
    ("asia-pacific/", "Asia-Pacific"),
    ("tag/ukraine-russia-crisis/", "Ukraine-Russia Crisis"),
    ("womens-world-cup-2023", "Women's World Cup 2023"),
    ("economy/", "Economy"),
    ("opinion/", "Opinion"),
    ("tag/coronavirus-pandemic/", "Coronavirus Pandemic"),
    ("climate-crisis", "Climate Crisis"),
    ("investigations/", "Investigations"),
    ("interactives/", "Interactives"),
    ("features/", "Features"),
    ("gallery/", "Gallery"),
    ("tag/science-and-technology/", "Science and Technology"),
    ("sports/", "Sports"),
    # ("audio/podcasts", "Audio/Podcasts") ##not included
]

# Function to extract news links
def extract_news_links(soup, tag):
    news_links = []
    for news_link in soup.find_all('a', href=True):
        if tag in news_link['href'] or "/news/" in news_link['href']:
            news_links.append(news_link['href'])
    return news_links


# Dictionary to store categories and their corresponding links
category_links_dict = {}

# Dictionary to store tags and their corresponding links
tag_links_dict = {}

# Iterate through the tags and navigate to each tag page
for tag, category in tags_and_categories:
    tag_link = starter_link + tag
    print(f"Navigating to tag: {tag_link}")
    driver.get(tag_link)
    time.sleep(5)
    category_soup = BeautifulSoup(driver.page_source, 'html.parser')
    news_links = []  # Initialize an empty list
    seen_links = set()  # To keep track of seen links and avoid duplicates
    for _ in range(2):
        try:
            extracted_links = extract_news_links(category_soup, tag)
            for link in extracted_links:
                if link not in seen_links:  # Check for duplicates
                    news_links.append(link)
                    seen_links.add(link)  # Add to seen links
            show_more_button = driver.find_element(By.CLASS_NAME, "show-more-button")
            driver.execute_script("arguments[0].scrollIntoView();", show_more_button)
            show_more_button.click()
            time.sleep(5)
            category_soup = BeautifulSoup(driver.page_source, 'html.parser')
        except Exception as e:
            print(f"Error clicking 'Show more' button for '{category}':", e)
            break
    tag_links_dict[tag] = news_links


# Quit the browser after scraping is done
driver.quit()

# Iterate through the article links and extract information
for tag, links in tag_links_dict.items():
    for article_path in links:
        # Ensure the URL has the complete scheme
        if not article_path.startswith('http'):
            article_url = starter_link + article_path
        else:
            article_url = article_path

        ua = UserAgent()
        headers = {"User-Agent": ua.random}
        try:
            response = requests.get(article_url, headers=headers)
            response.encoding = "utf-8"
            soup = BeautifulSoup(response.text, "html.parser")

            # Extract headline
            headline = soup.select_one("#main-content-area > header > h1").get_text(strip=True)

            # Extract subheading
            subheading_element = soup.select_one("#main-content-area > header > p > em")
            subheading = subheading_element.get_text(strip=True) if subheading_element else ""

            # Extract author
            author_element = soup.select_one("#main-content-area > div.article-info-block.css-ti04u9 > div.article-b-l > div.article-author-name > span.article-author-name-item > a")
            author = author_element.get_text(strip=True) if author_element else ""

            # Extract publish date
            date_element = soup.select_one("#main-content-area > div.article-info-block.css-ti04u9 > div.article-b-l > div > div > span:nth-child(2)")
            date_string = date_element.get_text(strip=True) if date_element else ""

            # Process the date_string and convert it to the desired format
            parsed_date = datetime.strptime(date_string, "%d %b %Y")
            publish_date = parsed_date.strftime("%Y-%m-%dT%H:%M:%SZ")

            # Extract article text
            article_text_element = soup.select_one("#main-content-area > div.wysiwyg.wysiwyg--all-content.css-ibbk12")
            article_text = "\n".join([p.get_text(strip=True) for p in article_text_element.find_all("p")])

            # Extract image source using JavaScript path
            img_elements = soup.select("#main-content-area > figure > div > img")
            img_desc_elements = soup.select("#main-content-area > figure > figcaption")

            # Generate a unique article ID using shortuuid
            article_id = shortuuid.uuid()

            # Specify the directory where you want to save the JSON file
            base_dir = "D:/image bias"
            image_folder = os.path.join(base_dir, article_id)

            # Create the directory if it doesn't exist
            os.makedirs(image_folder, exist_ok=True)

            # Create a JSON dictionary to store the article information
            JSON_dict = {
                "url": article_url,
                "heading": headline,
                "subheading": subheading,
                "text": article_text,
                "author": author,
                "publish_date": publish_date,
                "id": article_id
            }

            # Save the JSON dictionary to a JSON file
            with open(os.path.join(image_folder, "article.json"), "w") as outfile:
                json.dump(JSON_dict, outfile, indent=4)

            # Print the JSON data
            json_str = json.dumps(JSON_dict, indent=4)
            print(json_str)

            # Download and save images with image description as filenames
            for i, img_element in enumerate(img_elements):
                img_src_relative = img_element.get("src")
                img_desc = img_desc_elements[i].get_text(strip=True) if i < len(img_desc_elements) else ""

                if img_src_relative:
                    # Convert the relative URL to an absolute URL
                    img_src_absolute = urljoin(article_url, img_src_relative)

                    # Remove invalid characters from the image description to ensure valid filenames
                    img_desc = "".join([c if c.isalnum() or c in (' ', '_', '-') else '' for c in img_desc])
                    img_desc = img_desc.strip()  # Remove leading/trailing spaces
                    img_desc = img_desc[:255]  # Limit filename length to 255 characters (a common limit)
                    img_filename = f"{img_desc}.jpg"  # Add file extension

                    # Download and save the image with the cleaned image description as the filename
                    img_response = requests.get(img_src_absolute)
                    with open(os.path.join(image_folder, img_filename), "wb") as img_file:
                        img_file.write(img_response.content)

        except Exception as e:
            print("Error extracting article details:", e)


