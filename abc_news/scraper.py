import datetime
import requests
from bs4 import BeautifulSoup
import os
from fake_useragent import UserAgent
import json
import shortuuid
from dateutil import parser, tz
import argparse
import shutil
import random


# update x.5
# transforming the scraper into a script so the link could be called from the main script
arg_parser = argparse.ArgumentParser(
    prog="help",
    description="This script takes one article link from abc news and scrapes the article \n"
                " and saves it as a folder with images and json file",
    epilog="If there are problems contact amirreza.alise@gmail.com"
)
arg_parser.add_argument("-l", "--Link", required=True,
                    help="The link to the article \n"
                         "eg. https://abcnews.go.com/US/110-million-alert-severe-dangerous-weather-holiday-weekend/story?id=100573102")
args = arg_parser.parse_args()

path = args.Link
#path = str(input("Enter the URL of the article: "))
# eg. https://abcnews.go.com/US/110-million-alert-severe-dangerous-weather-holiday-weekend/story?id=100573102


## update x.6
# building proxy list to avoid getting blocked
def LoadUpProxies():
    try:
        proxies = []
        url = 'https://sslproxies.org/'
        ua = UserAgent()
        header = {"User-Agent": ua.random}
        response = requests.get(url, headers=header)
        soup = BeautifulSoup(response.content, 'html.parser')
        soup2 = soup.find("div", class_="table-responsive fpl-list")
        soup3 = soup2.find("table", class_="table table-striped table-bordered").find("tbody").find_all("tr")[1:20]
        for i in soup3:
            try:
                ip = i.find_all('td')[0].string
                port = i.find_all('td')[1].string
                proxy = {'ip': ip, 'port': port}
                proxies.append(proxy)
            except:
                print('')
    except:
        proxies = [{'ip': '190.61.88.147', 'port': '8080'}, {'ip': '129.159.112.251', 'port': '3128'},
                   {'ip': '186.121.235.66', 'port': '8080'}, {'ip': '116.111.222.9', 'port': '23775'},
                   {'ip': '167.172.80.53', 'port': '3128'}, {'ip': '64.225.4.63', 'port': '9990'},
                   {'ip': '173.176.14.246', 'port': '3128'}, {'ip': '178.33.3.163', 'port': '8080'},
                   {'ip': '20.44.206.138', 'port': '80'}, {'ip': '129.153.157.63', 'port': '3128'},
                   {'ip': '158.160.56.149', 'port': '8080'}, {'ip': '186.121.235.222', 'port': '8080'},
                   {'ip': '3.141.13.89', 'port': '80'}, {'ip': '116.111.217.187', 'port': '23775'},
                   {'ip': '20.210.26.214', 'port': '3333'}, {'ip': '116.111.218.37', 'port': '23775'},
                   {'ip': '64.225.4.29', 'port': '9865'}, {'ip': '64.225.8.135', 'port': '9995'},
                   {'ip': '8.219.97.248', 'port': '80'}]
    return proxies


ua = UserAgent()
headers = {"User-Agent": ua.random}
try:
    # update x.6
    proxies = LoadUpProxies()
    proxy_index = random.randint(0, len(proxies) - 1)
    proxy_ip = proxies[proxy_index]['ip']
    proxy_port = proxies[proxy_index]['port']
    ###
    doc = requests.get(args.Link, headers=headers, proxies={"http": f"http://{proxy_ip}:{proxy_port}"}) # update x.6
    doc.encoding = "utf8"
    doc = BeautifulSoup(doc.text, "html.parser")
    pass

except:
    print("The URL or file name is invalid.")
    pass

# headline
try:
    headline = doc.find("h1").string
    pass
except:
    headline = ""
    pass

# subheading
try:
    subheading = doc.find("h1").find_parent("div").find("p").string
    pass
except:
    subheading = ""
    pass


# author
def author_finder(doc):
    # authors are in a div with data-testid="prism-byline"
    try:
        authors = doc.find("div", {"data-testid": "prism-byline"}).find_all("a")
        # author = ",".join([a.string for a in authors])
        if len(authors) == 1:
            author = [authors[0].string]
        else:
            author = [a.string for a in authors]
        pass

    except:
        author = []
        pass

    return author


author = author_finder(doc)

# date
# formatted according to ISO 8601 eg. 2021-05-29T00:00:00+10:00
# abc news website reports the time according to the scrapers local time
# example time data from abc: July 1, 2023, 7:07 PM
try:
    date = doc.find("div", class_="Zdbe").string # July 6, 2023, 7:07 PM
    date_obj = parser.parse(date) # 2023-07-06 19:07:00
    # convert to UTC
    date_utc = date_obj.astimezone(tz.tzutc()).isoformat() # 2023-07-06 09:07:00+00:00
    date_iso8601 = date_utc.split("+")[0] + "Z"

except:
    date_iso8601 = ""
    pass

# article body
try:
    article_body = doc.find("article", {"data-testid": "prism-article-body"}).find_all("p")
    text = "".join([p.text for p in article_body])
    pass
except:
    text = ""
    pass

id = shortuuid.uuid()
#path_name = slugify(headline, allow_unicode=True)
now = datetime.datetime.now().strftime("%Y-%m-%d")

'''
# update 1.5.2 (auto scraping)
dest_time_path = f"{now}/"
if not os.path.exists(dest_time_path):
    os.makedirs(dest_time_path)
    pass
dest_path = f"{now}/{id}/"
###
'''

dest_path = f"{id}/"
if headline != "404":
    if not os.path.exists(dest_path):
        os.makedirs(dest_path)
    else:
        print("The article with this id already exists.")
        pass
    pass
else:
    print("error 404")

valid_imgs = []

pictures = doc.find_all("figure")
for picture in pictures:
    img = picture.find("img")
    img_src = img["src"]
    img_name = img_src.split("/")[-1].split("?")[0]

    try:
        img_data = requests.get(img_src, headers=headers).content

        # removing "." from the filename except the last one
        img_name = img_name.replace(".", "", img_name.count(".") - 1)

        print(f"Downloading {img_name}  ...")
        # we download the image with path img_src into a dist_path and adding .png extension
        with open(f"{dest_path}{img_name}", "wb") as file:
            file.write(img_data)

        try:
            img_desc = img["alt"]
            if img_desc is None or img_desc == "":

                fig_cap = img.find("figcaption").find("div").find_all("div")[1].find("span").find("span").find("span")
                img_desc = fig_cap.string

            image = {"filename": img_name, "description": img_desc}
        except:
            img_desc = None
            image = {"filename": img_name}
            pass

        valid_imgs.append(image)

        pass

    except:
        pass


for img in valid_imgs:
    try:
        if img["description"] == None:
            valid_imgs.remove(img)
            pass
        pass
    except:
        pass
    pass

# tags
try:
    tags = doc.find("div", {"data-testid": "prism-tags"}).find("ul").find_all("li")
    tags = [t.find("a").find("span") for t in tags]
    # tags = ",".join([t.string for t in tags])
    if len(tags) == 1:
        tags = [tags[0].string]
        pass
    else:
        tags = [t.string for t in tags]
    pass
except:
    tags = []
    pass

# update x.4
# if there is no image, delete the folder and stop the program
# execute this part with any cost
finally:
    if not len(valid_imgs) != 0:
        print(f"There are no images in this article. Deleting folder {dest_path} ...")
        shutil.rmtree(dest_path)
        exit()

JSON_dict = {
    "url": path,
    "heading": headline,
    "subheading": subheading,
    "text": text,
    "author": author,
    "publish_date": date_iso8601,
    "tags": tags,
    "img_desc": valid_imgs,
    "id": id
}

with open(f"{dest_path}article.json", "w") as outfile:
    json.dump(JSON_dict, outfile, indent=4)
