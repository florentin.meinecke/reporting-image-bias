import os
import time

with open("cache/clean_links.txt", "r") as file:
    article_links = file.readlines()
    pass

print(f"{len(article_links)} possible articles to download, do you want to continue?")

# update x.7.2
def download_articles(start, target, step_size=50, indx=0):

    if indx == 0:
        indx = start

    end = start + step_size
    if end > target:
        end = target

    range1 = range(start, end)

    '''
    agreement = input(f"Enter Y to continue with {range1} and N to stop: ")
    if agreement == "Y":
        pass
    if agreement == "N":
        exit()
    '''

    # trying the scraper on the links
    # article extraction
    for link in article_links[start:end]:
        print(f"link {link}")
        print(f"working on the link number {indx}")
        os.system(f"python scraper.py -l {link}")
        #time.sleep(0.1)
        indx += 1
        pass

    start = end

    while True:
        download_articles(start, target, step_size=step_size, indx=indx)
        pass


download_articles(9000, len(article_links), step_size=50, indx=0)
