# How to use scrapers
### according to update x.5: 
    example scraper: abc_news
    
    install the requirements
    run the python files in this order:
    1_extract_links.py, adjust the number links you are looking to extract.
    2_clean_kinks.py to clean the links.
    3_download_articles to download the articles in desired folder.
    4_remove_duplicates.py to remove duplicates.
    5_check_formats.py to check the formats of the articles.
