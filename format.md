# Format Guidelines

## Repo Directory Structure

In the main folder, some base files exist that concern the entire project, such as README.md and this formatting guide.

Apart from these, all files should be put in suitable sub-directories. These will be uploaded not to gitlab, but to: https://1drv.ms/f/s!AkgwFZyClZ_qk-lxr_8_OirFYyYPYA?e=yQHB8a.

For each media outlet, a subfolder should me made with a name referring to the respective outlet (e.g. a folder titled "huffpost" for the https://www.huffpost.com/ scraper).
Within this folder should be
- a python script called `scraper.py` with the core scraping functionality
- a info/readme file explaining the function of the scraper, and possible other files/folders
- possibly an additional helpers script or folder for functions or config to import

## Scraping Data Structure

### Outlet Folders

For the output of each scraper, we need a folder titled the same name as the folder in the repo (e.g. "huffpost") for all the articles scraped from the source.

### Article Folders

Each article should then get a subfolder inside, named with an unique ID. Generate unique ID via python's `shortuuid`.
```python
import shortuuid
id = shortuuid.uuid()
``` 

Inside the article subfolder, the article text (including title and metadata) and all attached images should be saved.

#### Images

Images should get unique titles, which could just be a number, or a short string. Crucially they should be named in a way where they can be cross-referenced with the information in the text file, as explained in the next chapter.

#### Text

The text should be formatted in a JSON, so we can distinguish heading, subheading, article text, author and others  automatically.

Proposed keywords are
- url: the reference link for the article
- heading: the main title of the article
- subheading: oftentimes a short summay of the main topic
- text: the entirety of the article text, or the preview in case a paywall hides the full text
- author: the name of the articles author
- publish_date: the initial upload date of the article (we don't regard later edits at this point in time), formatted according to [ISO 8601 in UTC](https://en.wikipedia.org/wiki/ISO_8601)
- tags: in our example article, that could be the keywords under "related" at the end of the article
- img_desc: in case images have descriptions or titles, those should be saved here with a reference to the image name
- id: unique identifier of the article, the same as which the article subfolder is named after. Use the python's shortuuid library

Some of those keywords do not have to be in the JSON file for it to still pass our formatting schema.
We require, in all cases, the url, heading, text and image fields.

Any other fields should be included if possible, but if the publishing date for example is not known it can be left out of the JSON file.

For image descriptions, a nested list of objects is proposed.
Here, each object has one required field, the filename.
This is a string that corresponds with the repective file in the directory.
Another optional field is description, that is any text found on the site that is directly attached to that image. In case none is found, the description field sohlud be left out here.


## Example

Going back to the example article https://www.huffpost.com/entry/joe-biden-bidenomics_n_649c8effe4b038eea76d01b3, our folder structure should be:


```
bias_article_mining
└── huffpost
    └── N7qFL4Xn3fqXcfedwzUPLM
        ├── 01.jpg
        ├── 02.jpg
        └── article.json
    └── [other_article]
└── [other_outlet]
    └── [other_article]
```


Then, the article.json file should look like this (text abrieviated to just the first paragraph for the example):

```json
{
    "url": "https://www.huffpost.com/entry/joe-biden-bidenomics_n_649c8effe4b038eea76d01b3",
    "heading": "In Speech On Bidenomics, Biden Tries To Turn The Page On Reagan (And Every President Since)",
    "subheading": "Biden has for months been telegraphing a major shift in economic policy, and on Wednesday, made the case for a new way of thinking.",
    "text": "In a major speech on Wednesday outlining his economic policy, Joe Biden took a page from his old pal Barack Obama. When Obama ran for reelection in 2012, he embraced the pejorative name given to his signature policy achievement — Obamacare. In Chicago today, Biden embraced the insult the Wall Street Journal Opinion page gave to his economic plans — Bidenomics. [...]",
    "author": [
        "Paul Blumenthal",
        "SECOND_AUTHOR"
    ],
    "publish_date": "2023-06-28T16:03:00.000Z",
    "tags": [
        "joe_biden",
        "economy",
        "budget_tax_and_economy",
        "economic_policy"
    ],
    "img_desc": [
        {"filename":"01.jpg"},
        {"filename":"02.jpg","description":"President Joe Biden speaks about Bidenomics at the Old Post Office in Chicago, Illinois, on June 28, 2023."}
    ],
    "id":"N7qFL4Xn3fqXcfedwzUPLM"
}
```

Here, the first image has no description, as it is the thumbnail of the article (which we include in image mining), whereas the second image is from within the article and has an attached description.
