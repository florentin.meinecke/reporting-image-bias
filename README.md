# Reporting-Image-Bias

## About the project

Different media outlets use different images to report on the same content (but from different perspectives).
We would like to build a dataset that collects many articles, along with text, title, metadata, and images, from various media outlets (classified into political orientations using the websites Allsides and AdFontes), always on similar topics.
Then, we annotate the extent to which the image induces bias and what exactly is seen in the image.

## Media Sources

The goal is to collect articles and images from a variety of media outlets, for which a framing bias from left- to right-wing alignment is known.
For this, sites like the following are used:
- https://www.allsides.com/
- https://adfontesmedia.com/

For each outlet that is chosen, a web-scraper should be implemented that collects a wide range of articles from the source.
To choose articles that can be scraped, different strategies may apply for different outlets, such as going through a feed, using "similar/recommended articles" links, or search engines.

### Step 1: Scraping and saving

So, the first step is to build several scrapers and apply them to different media outlets that we want to scrape.

This repository is meant to aggregate the scrapers our team members build for various news sites, and to coordinate work on them.
As the scrapers will collect both text and images from the articles, it will be necessary to create a schema on how to save the scraped data in a uniform manner, to facilitate analysis.
For this, a guideline for formatting is shared in this repository (format.md in the main folder), which should be adhered to when saving scraped data.

The scraped articles should be saved/uplodaded to: https://1drv.ms/f/s!AkgwFZyClZ_qk-lxr_8_OirFYyYPYA?e=yQHB8a.

It is recommended to check this guideline every now and again, to make sure the scrapers still adhere to it, in case something is changed.

### Step 2: Annotation

In the second step, annotation takes place. The process will involve two steps: annotating the text and title, and annotating the image. 
 
**Definitions of Bias applicable to our annotation:**

1. Racial Bias refers to the favoritism or prejudice expressed towards people on the basis of their race or ethnicity. In the context of news articles, racial bias may present itself in the form of:

Stereotyping: The application of generalized beliefs or attitudes about a particular racial or ethnic group to individuals from that group.

Discrimination: Unfair treatment of individuals based on their race or ethnicity.

Underrepresentation or Overrepresentation: The unequal or disproportional representation of racial or ethnic groups in stories or images.

2. Hate speech is any communication that disparages a person or a group on the basis of some characteristic such as race, color, ethnicity, gender, sexual orientation, nationality, religion, or other characteristics. Hate speech can take many forms, including:

Derogatory Language: Using insults, slurs, or derogatory terms directed at a specific group of people.

Stereotypes: Generalized and oversimplified beliefs about a particular group that are negative and demeaning.

Incitement: Speech aiming to incite violence or prejudicial actions against a specific group.

Dehumanization: Language that devalues individuals based on their group identity, reducing them to less than human.

3. General bias involves favoritism or prejudice expressed on the basis other than race, such as the portrayal of events, individuals, or issues in a manner that favors a particular political party, gender, or stance.

When you annotate an article and the images in the article, there are two main steps for the annotation: images and text. 

**Image Annotation:**

1. Analyze each image carefully, considering the content, focus, and context.
2. Consider whether the image accurately represents the event or subject of the article, and whether it could be perceived as being biased in any way.
3. Write a brief note explaining whether you see the image as not biased, or biased within either general bias, hate speech, or racial bias. 
4. Briefly describe in one sentence how the bias is manifested, and why you perceive it as bias.

**Text Annotation:**

1. Carefully read through the entire article, including the headline and any captions.
2. Mark whether you generally perceive the article as being biased or not biased. 
3. If biased, mark whether you particularly identify general bias, hate speech, or racial bias. 
4. Briefly describe in one sentence how the bias is manifested, and why you perceive it as bias.


## Workflow

### Choice of media outlet

Who works on which media outlet may be decided by each student themselves.
To avoid duplicate work, it is recommended to check this repository frequently to see what folders for scrapers already exist.
This is our authorative info on who works on what.

For example, if I chose to work on the Huffpost web-scraper, i would as a first step create a "huffpost" subfolder in the repo, with a placeholder file (eg hello_world.py) and then push that here, so everyone can see a project folder for the Huffpost has been created by me.

Of course, other ways of communication can also be used, but we will consider the project folders in this repo as our primary source.

### Working within the repository

If you are a student who has been assigned to work on this project (or you chose to on your own), you should
- either already have a token sent to you, or request one from the maintainer
- clone this repo to your workstation,
- work on the web scraper that you were assigned or chose,
- make sure to commit changes to the scraper frequently and with meaningful commit messages,
- then push the changed files to this repo again.

Your commit messages should reflect what part of the scraper you made changes in.

The commands required (on UNIX-like systems) should be

```
#for the first time getting this repository
git clone https://[your_token]@gitlab.gwdg.de/florentin.meinecke/reporting-image-bias
#for any further synchronising, once you have a local copy from cloning
git pull -origin
#edit your scraper, then
git add [scraper_folder]
git commit -m [meaningful_message]
git push -u origin main
```

## Contact

This repository is made and maintained by Florentin Meinecke.

For any questions regarding the repository itself, please send an [email](mailto:florentin.meinecke@stud.uni-goettingen.de) to me.

For questions about project details, please refer either to me, or directly to [Timo Spinde](mailto:timo.spinde@uni-goettingen.de), who is spearheading this project.

Alternatively, you can reach out on [RocketChat](https://chat.gwdg.de), either to me or Timo directly, or in the "Bias_In_Images_Dataset" group.

