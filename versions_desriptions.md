This is a short explanation on how scrapers have been assigned a version and what each number means

example: `ver. 1.3.1.2306`
-
- 1: The first number determines the functionality of the scraper 0 means it is a scraper is not returning a desirable output and 1 means it is returning at least one correct output
- 2: The second number determines the output format of the scraper according to the format.md
- 3: The third number shows small modifications locally made to the scraper
- 4 - < date >
  - this number determines the date that the scraper was updated
  - eg.`1.5.2.2307`which means the scraper was updated on July 2023

Update log:
- 
- x.8: added archive.txt file to list all previously downloaded articles, so they won't be downloaded again
- x.7: more dynamic functions added to scraper, link_extractor, and downloader
- x.6: proxies added to prevent getting blocked by the website
- x.5:
  - scraper is now a script that can be run from the command line with respecctful arguments for example:
    - `python3 scraper.py https://abcnews.go.com/US/wife-gilgo-beach-murder-suspect-files-divorce-court/story?id=101503850`
- x.4: 
  - time.sleep(5) added to give time for the page to load
  - added a try except to catch the error when the page does not load
  - if the scraper fails to extract at least one image for the article it will delete the article (this feature required for external scripts for mass tries)
- x.3:
    - time formats fixed