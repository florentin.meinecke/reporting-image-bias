import json
import shutil

import jsonschema
import os
import sys

def get_article_schema():
	'''
	doc will be added (:
	'''
	with open('article_schema.json','r') as f:
		return(json.load(f))

def write_json(path,data):
	with open(path,'w') as f:
		f.write(json.dumps(data,indent=4))

def read_json(path):
	with open(path,'r') as f:
		return(json.load(f))

def print_keywords():
	#schema = get_article_schema()
	devnull=[print(k) for k in schema['properties'].keys()]

def check_json_format(data):
	'''
	This function tests a python dict that should constitue a valid "article"
	for the purposes of saving it from the scraper.
	It takes in a dict and pulls additional information about the required schema.
	Returns either 'valid' if no issues are found,
	or an error description that informs the user as to what should be corrected.
	refer to https://github.com/python-jsonschema/jsonschema
	and further links from there for detailed docs.
	'''
	#schema = get_article_schema()
	checker = jsonschema.FormatChecker()
	try:
		jsonschema.validate(instance=data,schema=schema,format_checker=checker)
		x = 'valid'
	except jsonschema.exceptions.ValidationError as e:
		x = f"Error in key '{e.path[0]}': {e.message}"
	return(x)

def check_article_folder(uuid):
	try:
		#TODO: ensure that we are in outlet directory.
		files = os.listdir(uuid)
	except FileNotFoundError:
		print(f'folder {uuid} could not be found - This should not be possible, please contact florentin.')
		return False
	try:
		article = read_json(os.path.join(uuid, 'article.json'))
	except FileNotFoundError:
		print(f'article.json for {uuid} folder not found.')
		return False
	json_check = check_json_format(article)
	if json_check != 'valid':
		print(json_check)
		return False
	image_names = [img['filename'] for img in article['img_desc'] ]
	for img in image_names:
		if img not in files:
			print(f'name {img} is not a file in {uuid} directory.')
			return False
		if len(img.split('.')) != 2:
			print(f'Please only have one . in JSON filename for image {img}')
			return False
		suffix = img.split('.')[1]
		if suffix.lower() not in ['jpg','jpeg']:
			print(f'only jpg images are accepted - please convert the images accordingly.')
			return False
	for f in files:
		if f == 'article.json':
			continue
		if f not in image_names:
			print(f'file {f} is not present in img_desc list in JSON file.')
			return False
		if len(f.split('.')) != 2:
			print(f'Please only have one . in filename for {f}')
			return False
		suffix = img.split('.')[1]
		if suffix.lower() not in ['jpg','jpeg']:
			print(f'only jpg images are accepted - please convert the images accordingly.')
			return False
	print('everything seems to be fine.')
	return True

def check_outlet_folder(outlet):
	print('')
	os.chdir(os.path.join(os.pardir,os.pardir))
	if os.path.basename(os.getcwd()) != 'reporting-image-bias':
		print('script has to be run from its own folder.')
		return False
	try:
		articles = os.listdir(outlet)
		articles = [a for a in articles if '.py' not in a and '.ipynb' not in a and a != 'cache']
	except FileNotFoundError:
		print(f'folder {outlet} could not be found - is there a typo?')
		return False
	os.chdir(outlet)
	invalid = 0
	for uuid in articles:
		article_valid = check_article_folder(uuid)
		if not article_valid:
			print(f'\narticle {uuid} has had issues, please check printouts.')
			invalid += 1
			# moving invalid articles to invalid folder
			if not os.path.exists(os.path.join(os.pardir, f'{outlet}/cache/invalid')):
				os.mkdir(os.path.join(os.pardir, f'{outlet}/cache/invalid'))
			shutil.move(uuid, os.path.join(os.pardir, f'{outlet}/cache/invalid'))
		continue
	os.chdir(os.pardir)
	if invalid > 0:
		print(f'{invalid} articles have had issues, please check printouts.')
		return False

	print(f'All articles in {outlet} are formatted correctly, yay :)')
	return True

#yikes
#TODO: refactor away from global variable asap
schema = get_article_schema()

if __name__ == '__main__':
	valid = check_outlet_folder(sys.argv[1])
