import os
import requests
from bs4 import BeautifulSoup
import json
from selenium import webdriver
import time
import shortuuid  # Import shortuuid library for generating unique folder names

# Define the starter link
base_link = 'https://thefederalist.com/latest/page/'

# Set the number of pages to scrape
total_pages_to_scrape = 1  # Change this to the desired number of pages

# Launch a web browser
driver = webdriver.Chrome()  # You can use other browsers too

# Function to extract and store article links
def extract_and_store_links(driver, links_dict):
    try:
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        article_containers = soup.find_all('div', class_='col-12 col-lg-6 col-xl-3 mb-30')

        for container in article_containers:
            article_link = container.find('a', href=True)
            href = article_link.get('href')
            if href and href.startswith('https://thefederalist.com/'):
                links_dict[href] = True  # Store the link in the dictionary

    except Exception as e:
        print("Error extracting article links:", e)

# Function to create a unique folder for each article
def create_article_folder():
    folder_name = shortuuid.uuid()
    folder_path = os.path.join("D:/image_bias", folder_name)
    os.makedirs(folder_path)
    return folder_path

# Dictionary to store article links
links_dict = {}

# Function to extract tags from an article
def extract_tags(article_url):
    try:
        # Visit the article URL
        driver.get(article_url)
        time.sleep(2)  # Reduce the delay for page loading

        # Create a BeautifulSoup object
        soup = BeautifulSoup(driver.page_source, 'html.parser')

        # Find the specified element containing the tags
        tags_container = soup.select_one("#article-container > div:nth-child(2) > div.col-12.col-md-11.col-lg-10.col-xl-6 > div.article-body.body-md.bdr-top-black.pt-30 > div.article-tags.bdr-top-black.mt-30.mt-sm-60.pt-15.pt-md-45 > ul")

        # Extract tags from the <ul> element
        tags = []
        if tags_container:
            tag_elements = tags_container.find_all('li')
            tags = [tag.text.strip() for tag in tag_elements]

        return tags

    except Exception as e:
        print("Error extracting tags:", e)
        return []

# Function to extract article details
def extract_article_details(article_url, folder_path):
    try:
        # Visit the article URL
        driver.get(article_url)
        time.sleep(2)  # Reduce the delay for page loading

        # Create a BeautifulSoup object
        soup = BeautifulSoup(driver.page_source, 'html.parser')

        # Find the specified element containing the paragraphs
        article_element = soup.find('div', class_='article-body')

        # Extract all paragraphs and concatenate into a single string
        paragraphs = []

        if article_element:
            paragraph_elements = article_element.find_all('p')

            for paragraph in paragraph_elements:
                paragraphs.append(paragraph.get_text(strip=True))

        # Concatenate paragraphs into a single string
        article_text = '\n'.join(paragraphs)

        # Find the div element with class "article-meta-author"
        author_div = soup.find('div', class_='article-meta-author')

        # Find the <span> element within the div
        author_span = author_div.find('span')

        # Extract the author's name from the <span> element
        author = author_span.text.strip() if author_span else ''

        # Extract the title using the class selector
        title_element = soup.find('h1', class_='title-lg')

        # Extract the title text
        title = title_element.text.strip() if title_element else ''

        # Use the first JavaScript selector to extract publish date
        publish_date_element = driver.execute_script('return document.querySelector("#article-container > div.row.justify-content-center.justify-content-lg-start > div > header > div.article-meta.d-block.d-md-flex.align-items-center.label-sm.mx-n20 > div.article-meta-date.mb-10.mb-md-0.px-20 > time")')

        # If the first selector doesn't work, try the second one
        if not publish_date_element:
            date_element = driver.execute_script('return document.querySelector("#article-container > div:nth-child(1) > div > header > div.article-meta.d-block.d-md-flex.align-items-center.justify-content-center.label-sm.mx-n20 > div.article-meta-date.mb-10.mb-md-0.px-20 > time")')
            publish_date = date_element.get_attribute('datetime') if date_element else ''
        else:
            publish_date = publish_date_element.get_attribute('datetime') if publish_date_element else ''

        # Use BeautifulSoup to find the image element
        image_element = soup.select_one('img.img-fluid.w-100.wp-post-image')

        # Extract image source using JavaScript selector
        image_element_js = driver.execute_script('return document.querySelector("#article-container > div:nth-child(2) > div > figure > img")')
        image_src_js = image_element_js.get_attribute('src') if image_element_js else ''

        # Extract image source using BeautifulSoup (fallback)
        image_src = image_element['src'] if image_element else image_src_js

        # Find the specified element containing the tags using BeautifulSoup
        tags_container = soup.select_one("#article-container > div:nth-child(3) > div > div.article-body.body-md.mt-30.mt-sm-60 > div.article-content > div.article-tags.bdr-top-black.mt-30.mt-sm-60.pt-15.pt-md-45 > ul")

        # Extract tags using BeautifulSoup or JavaScript selector as a fallback
        tags = []
        if tags_container:
            tag_elements = tags_container.find_all('li')
            tags = [tag.text.strip() for tag in tag_elements]

        if not tags:
            # Use JavaScript selector to extract tags
            tags_element = driver.execute_script('return document.querySelector("#article-container > div:nth-child(3) > div > div.article-body.body-md.mt-30.mt-sm-60 > div.article-content > div.article-tags.bdr-top-black.mt-30.mt-sm-60.pt-15.pt-md-45 > ul")')
            tags = tags_element.text.strip() if tags_element else ''

        # Convert tags to a list
        if isinstance(tags, str):
            tags = [tag.strip() for tag in tags.split('\n') if tag.strip()]

        # Print the extracted information
        # print("URL:", article_url)
        # print("Headline:", title)
        # print("Author:", author)
        # print("Publish Date:", publish_date)
        # print("Image Source URL:", image_src)
        # print("Tags:", tags)

        # Create a dictionary with article details
        article_details = {
            "url": article_url,
            "headline": title,
            "author": author,
            "publish_date": publish_date,
            "article_text": article_text,  # Store as a single string
            "img_src": image_src,
            "tags": tags
        }

        # Save the article info as a JSON file within the article's folder
        json_file_path = os.path.join(folder_path, "article_info.json")
        with open(json_file_path, "w", encoding="utf-8") as json_file:
            json.dump(article_details, json_file, indent=4)

        # Save the article image (if available) to a file
        if image_src:
            response = requests.get(image_src)
            with open(os.path.join(folder_path, "article_image.jpg"), "wb") as img_file:
                img_file.write(response.content)

    except Exception as e:
        print("Error extracting article details:", e)

# Loop through the specified number of pages to scrape
for page_number in range(1, total_pages_to_scrape + 1):
    page_url = base_link + str(page_number)
    driver.get(page_url)
    time.sleep(2)  # Delay to ensure page loads completely
    extract_and_store_links(driver, links_dict)

# Loop through the collected article links and extract details
for article_url in links_dict.keys():
    # Call the function to create a unique folder for the article
    folder_path = create_article_folder()

    # Call the function to extract article details and save to JSON file
    extract_article_details(article_url, folder_path)

# Quit the browser after scraping is done
driver.quit()
