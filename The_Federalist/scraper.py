import argparse
import random
import shutil
import requests
import shortuuid
from bs4 import BeautifulSoup
import os
from fake_useragent import UserAgent
import json
from dateutil import parser, tz
import time


# update x.5
# transforming the scraper into a script so the link could be called from the main script
arg_parser = argparse.ArgumentParser(
    prog="help",
    description="This script takes one article link from abc news and scrapes the article \n"
                " and saves it as a folder with images and json file",
    epilog="If there are problems contact amirreza.alise@gmail.com"
)
arg_parser.add_argument("-l", "--Link", required=True,
                    help="The link to the article \n"
                         "eg. https://abcnews.go.com/US/110-million-alert-severe-dangerous-weather-holiday-weekend/story?id=100573102")
args = arg_parser.parse_args()

path = args.Link


#path = "https://thefederalist.com/2023/07/21/atom-bomb-father-j-robert-oppenheimer-i-am-become-death-the-destroyer-of-worlds/"
#path = str(input("Enter the URL of the article: "))
# eg. https://abcnews.go.com/US/110-million-alert-severe-dangerous-weather-holiday-weekend/story?id=100573102


## update x.6
# building proxy list to avoid getting blocked
def LoadUpProxies():
    try:
        proxies = []
        url = 'https://sslproxies.org/'
        ua = UserAgent()
        header = {"User-Agent": ua.random}
        response = requests.get(url, headers=header)
        soup = BeautifulSoup(response.content, 'html.parser')
        soup2 = soup.find("div", class_="table-responsive fpl-list")
        soup3 = soup2.find("table", class_="table table-striped table-bordered").find("tbody").find_all("tr")[1:20]
        for i in soup3:
            try:
                ip = i.find_all('td')[0].string
                port = i.find_all('td')[1].string
                proxy = {'ip': ip, 'port': port}
                proxies.append(proxy)
            except:
                print('')
    except:
        proxies = [{'ip': '190.61.88.147', 'port': '8080'}, {'ip': '129.159.112.251', 'port': '3128'},
                   {'ip': '186.121.235.66', 'port': '8080'}, {'ip': '116.111.222.9', 'port': '23775'},
                   {'ip': '167.172.80.53', 'port': '3128'}, {'ip': '64.225.4.63', 'port': '9990'},
                   {'ip': '173.176.14.246', 'port': '3128'}, {'ip': '178.33.3.163', 'port': '8080'},
                   {'ip': '20.44.206.138', 'port': '80'}, {'ip': '129.153.157.63', 'port': '3128'},
                   {'ip': '158.160.56.149', 'port': '8080'}, {'ip': '186.121.235.222', 'port': '8080'},
                   {'ip': '3.141.13.89', 'port': '80'}, {'ip': '116.111.217.187', 'port': '23775'},
                   {'ip': '20.210.26.214', 'port': '3333'}, {'ip': '116.111.218.37', 'port': '23775'},
                   {'ip': '64.225.4.29', 'port': '9865'}, {'ip': '64.225.8.135', 'port': '9995'},
                   {'ip': '8.219.97.248', 'port': '80'}]
    return proxies


ua = UserAgent()
headers = {"User-Agent": ua.random}
try:
    # update x.6
    proxies = LoadUpProxies()
    proxy_index = random.randint(0, len(proxies) - 1)
    proxy_ip = proxies[proxy_index]['ip']
    proxy_port = proxies[proxy_index]['port']
    ###
    doc = requests.get(path, headers=headers, proxies={"http": f"http://{proxy_ip}:{proxy_port}"}) # end of update x.6
    time.sleep(1)
    doc.encoding = "utf8"
    doc = BeautifulSoup(doc.text, "html.parser")
    pass

except:
    print("The URL or file name is invalid.")
    pass

# headline
try:
    headline = doc.find("h1", class_="title-lg").string
    pass
except:
    headline = ""
    pass

# subheading
try:
    subheading = doc.find("div", class_="article-excerpt").find("p").string

except:
    subheading = ""
    pass

# author
def author_finder(doc):
    try:
        divs = doc.find("div", class_="article-meta-author").find_all("a")
        authors = [a.find("span") for a in divs]
        if len(authors) == 1:
            author = [authors[0].string]
        else:
            # all except the last one which is always FOX_news
            author = [a.string for a in authors]
        pass
    except:
        try:
            author = [] # placeholder

            pass
        except:
            try:
                author = [] # placeholder
                pass

            except:
                author = []
                pass

    return author


author = author_finder(doc)

# date
# date should be in iso8601 format and in UTC
try:
    date = doc.find("div", class_="article-meta-date").find("time")["datetime"] # eg.  2023-07-23
    date_obj = parser.parse(date)

    date_utc = date_obj.astimezone(tz.gettz("UTC")).isoformat() # eg. 2023-07-01 14:14:00+00:00
    # converting to iso8601 format

    date_iso8601 = date_utc.split("+")[0] + "Z" # eg. 2023-07-01T14:14:00Z
except:
    date_iso8601 = ""
    pass

# article body
try:
    article_body = doc.find("div", class_="article-content").find_all("p")
    text = "".join([p.text for p in article_body])
    pass
except:
    text = ""
    pass

id = shortuuid.uuid()
#path_name = slugify(headline, allow_unicode=True)
dest_path = f"{id}/"
if headline != "404":
    if not os.path.exists(dest_path):
        os.makedirs(dest_path)
    else:
        print("The article with this id already exists.")
        pass
    pass
else:
    print("error 404")

valid_imgs = []
pictures = doc.find_all("figure")
for picture in pictures:
    img = picture.find("img")
    img_src = img["src"]
    img_name = img_src.split("/")[-1].split("?")[0]

    # update x.x.3
    # removing extra dots in image name
    try:
        dots = img_name.count(".")
        format = img_name.split(".")[-1]
        name = img_name.split(".")[:-1]
        if dots > 1:
            img_name = "".join(name) + "." + format
            pass
    except:
        pass

    try:
        img_data = requests.get(img_src, headers=headers).content

        # update x.x.2 fixing JPG formatting
        existing_format = img_name.split(".")[-1]
        if existing_format != "jpg":
            img_name = img_name.replace(f".{existing_format}", ".jpg")
            pass

        print(f"Downloading {img_name}  ...")

        with open(f"{dest_path}{img_name}", "wb") as file:
            file.write(img_data)

        try:
            img_desc = None
            try:
                img_desc = img["alt"]
            except:
                pass

            if img_desc is None or img_desc == "":
                fig_cap = picture.find_parent("div").find("figcaption").find("div").find("div").find("a")
                img_desc = fig_cap.string

        except:
            img_desc = None

        if img_desc is not None or img_desc != "":
            image = {"filename": img_name, "description": img_desc}
        else:
            image = {"filename": img_name}

        valid_imgs.append(image)

        pass
    except:
        pass

# tags
try:
    tags_div = doc.find("div", class_="article-tags").find("ul", class_="list-unstyled").find_all("li")
    tags = [tag.find("a").string for tag in tags_div]
    pass
except:
    tags = []
    pass

# update x.4
if len(valid_imgs) == 0:
    # stop the program and delete the dest_path folder if there is no image
    print(f"There are no images in this article. Deleting folder {dest_path} ...")
    shutil.rmtree(dest_path)
    exit()

JSON_dict = {
    "url": path,
    "heading": headline,
    "subheading": subheading,
    "text": text,
    "author": author,
    "publish_date": date_iso8601,
    "tags": tags,
    "img_desc": valid_imgs,
    "id": id
}

with open(f"{dest_path}article.json", "w") as outfile:
    json.dump(JSON_dict, outfile, indent=4)

