import argparse
import random
import shutil

import requests
from bs4 import BeautifulSoup
import os
from fake_useragent import UserAgent
import json
from datetime import datetime, timedelta, timezone
import shortuuid


# update x.5
# transforming the scraper into a script so the link could be called from the main script
arg_parser = argparse.ArgumentParser(
    prog="help",
    description="This script takes one article link from abc news and scrapes the article \n"
                " and saves it as a folder with images and json file",
    epilog="If there are problems contact amirreza.alise@gmail.com"
)
arg_parser.add_argument("-l", "--Link", required=True,
                    help="The link to the article \n"
                         "eg. https://abcnews.go.com/US/110-million-alert-severe-dangerous-weather-holiday-weekend/story?id=100573102")
args = arg_parser.parse_args()

path = args.Link
#path = str(input("Enter the URL of the article: "))
# eg. https://nypost.com/2021/05/29/why-are-people-still-wearing-masks-outside/


## update x.6
# building proxy list to avoid getting blocked
def LoadUpProxies():
    try:
        proxies = []
        url = 'https://sslproxies.org/'
        ua = UserAgent()
        header = {"User-Agent": ua.random}
        response = requests.get(url, headers=header)
        soup = BeautifulSoup(response.content, 'html.parser')
        soup2 = soup.find("div", class_="table-responsive fpl-list")
        soup3 = soup2.find("table", class_="table table-striped table-bordered").find("tbody").find_all("tr")[1:20]
        for i in soup3:
            try:
                ip = i.find_all('td')[0].string
                port = i.find_all('td')[1].string
                proxy = {'ip': ip, 'port': port}
                proxies.append(proxy)
            except:
                print('')
    except:
        proxies = [{'ip': '190.61.88.147', 'port': '8080'}, {'ip': '129.159.112.251', 'port': '3128'},
                   {'ip': '186.121.235.66', 'port': '8080'}, {'ip': '116.111.222.9', 'port': '23775'},
                   {'ip': '167.172.80.53', 'port': '3128'}, {'ip': '64.225.4.63', 'port': '9990'},
                   {'ip': '173.176.14.246', 'port': '3128'}, {'ip': '178.33.3.163', 'port': '8080'},
                   {'ip': '20.44.206.138', 'port': '80'}, {'ip': '129.153.157.63', 'port': '3128'},
                   {'ip': '158.160.56.149', 'port': '8080'}, {'ip': '186.121.235.222', 'port': '8080'},
                   {'ip': '3.141.13.89', 'port': '80'}, {'ip': '116.111.217.187', 'port': '23775'},
                   {'ip': '20.210.26.214', 'port': '3333'}, {'ip': '116.111.218.37', 'port': '23775'},
                   {'ip': '64.225.4.29', 'port': '9865'}, {'ip': '64.225.8.135', 'port': '9995'},
                   {'ip': '8.219.97.248', 'port': '80'}]
    return proxies


ua = UserAgent()
headers = {"User-Agent": ua.random}
try:
    # update x.6
    proxies = LoadUpProxies()
    proxy_index = random.randint(0, len(proxies) - 1)
    proxy_ip = proxies[proxy_index]['ip']
    proxy_port = proxies[proxy_index]['port']
    ###
    doc = requests.get(args.Link, headers=headers, proxies={"http": f"http://{proxy_ip}:{proxy_port}"}) # end of update x.6
    doc.encoding = "utf8"
    doc = BeautifulSoup(doc.text, "html.parser")
    pass

except:
    print("The URL or file name is invalid.")
    pass

# headline
try:
    headline = doc.find("h1").string
    pass
except:
    headline = "headline not found"
    pass

# subheading
# New York Post does not have subheadings
try:
    subheading = doc.find("h1").find_parent("div").find("p").string
    if subheading is None:
        subheading = ""
    pass
except:
    subheading = ""
    pass


# author
def author_finder(doc):
    # authors are in a div with data-testid="prism-byline"
    try:
        authors = doc.find("div", class_="byline meta meta--byline").find_all("div", class_="byline__author")
        # author = ",".join([a.string for a in authors])
        if len(authors) == 1:
            author = [authors[0].find("a").string]
        else:
            author = [a.find("a").string for a in authors]
        pass

    except:
        author = []
        pass

    return author


author = author_finder(doc)

# date
def date_finder(doc):
    # formatted according to ISO 8601 eg. 2021-05-29T00:00:00+10:00
    # abc news website reports the time according to the scrapers local time
    # example time data from abc: July 1, 2023, 7:07 PM
    # New York Post is on UTC-4
    try:
        date = doc.find("div", class_="date meta meta--byline").find_all("span")[0].string # July 8, 2023
        hour = doc.find("div", class_="date meta meta--byline").find_all("span")[1].string # 7:07am
        concat_date = f"{date}, {hour}" # July 8, 2023, 7:07am


        date_iso = datetime.strptime(concat_date, "%B %d, %Y, %I:%M%p") # 2023-07-08 07:07:00
        # the date should be iso-utc formatted which is +4 hours from UTC
        date_utc = date_iso + timedelta(hours=4) # 2023-07-08 11:07:00
        # output format: 2023-06-28T16:03:00.000Z
        date_utc = date_utc.isoformat() # 2023-07-08T11:07:00
        date_iso8601 = date_utc + "Z" # 2023-07-08T11:07:00Z


        """
        date_list = date.split(",")  # ['July 1', '2023']
        month = date_list[0].split(" ")[0]  # ['July', '1']
        day = date_list[0].split(" ")[1]  # ['July', '1']
        date_new = f"{day} {month} {date_list[1]}"  # 1 July 2023
        print(date_new)
        # the date should be iso-utc formatted
        date_iso = datetime.strptime(date_new, "%d %B %Y")  # 2023-07-01 00:00:00
        # scraper_time_zone_offset = datetime.now(timezone.utc).astimezone().tzinfo.utcoffset(None)  # eg. +10:00:00
        # TODO: has to be converted to UTC, i'm just doing a dirty fix to check for conformity right now
        # date_iso8601 = date_iso.strftime(f"%Y-%m-%dT%H:%M:%SZ")#{scraper_time_zone_offset}") # eg.m
        date_iso8601 
        """
        pass
    except:
        date_iso8601 = ""
        pass

    return date_iso8601

date_iso8601 = date_finder(doc)

# article body
# there are some random <a> tags containing text in the article body
# we want to include them in the text
try:
    article_body = doc.find("div", class_="single__content entry-content m-bottom").find_all("p")
    text = "".join([p.text for p in article_body])
    """
    for p in article_body:
        a_present = p.find("a")
        if a_present:
            p = str(p.find_all("a").unwrap()) # removes the <a> tag but keeps the text
        par_text = p.text
        text += par_text + "\n"
    """
    pass
except:
    text = ""
    pass

id = shortuuid.uuid()
#path_name = slugify(headline, allow_unicode=True)
dest_path = f"{id}/"
if headline != "404":
    if not os.path.exists(dest_path):
        os.makedirs(dest_path)
    else:
        print("The article with this id already exists.")
        pass
    pass
else:
    print("error 404")

valid_imgs = []

pictures = doc.find_all("figure")
for picture in pictures:
    try:
        img = picture.find("img")
        img_src = img["src"]
        pass
    except:
        img = picture.find("a").find("img")
        img_src = img["src"]


    img_name = img_src.split("/")[-1].split("?")[0]

    try:
        img_data = requests.get(img_src, headers=headers).content
        print(f"Downloading {img_name}  ...")
        # we download the image with path img_src into a dist_path
        # update x.x.2 fixing JPG formatting
        existing_format = img_name.split(".")[-1]
        if existing_format != "jpg":
            img_name = img_name.replace(f".{existing_format}", ".jpg")
            pass

        with open(f"{dest_path}{img_name}", "wb") as file:
            file.write(img_data)

        try:
            img_desc = img["alt"]
            if img_desc is None or img_desc == "":

                fig_cap = img.find_all("figcaption")[0].string
                fig_credit = img.find_all("figcaption").find("span").string
                img_desc = fig_cap + " " + fig_credit

            image = {"filename": img_name, "description": img_desc}
        except:
            img_desc = None
            image = {"filename": img_name}
            pass


        valid_imgs.append(image)

        pass
    except:
        pass

# tags
# tags are in a ul aria-labelledby="filed-under"
try:
    tags = doc.find("ul", {"aria-labelledby": "filed-under"}).find_all("li")
    tags = [t.find("a") for t in tags]
    # tags = ",".join([t.string for t in tags])
    if len(tags) == 1:
        tags = [tags[0].string]
        print(f'only tag: {tags}')
        pass
    else:
        print('multiple tags found')
        tags = [t.string for t in tags[:-1]]
        # removing \n and \t
        try:
            tags = [t.replace("\n", "") for t in tags]
            tags = [t.replace("\t", "") for t in tags]
        except:
            pass
    pass
except:
    print('issue with tags')
    tags = ""
    pass

# update x.4
if len(valid_imgs) == 0:
    # stop the program and delete the dest_path folder if there is no image
    print(f"There are no images in thi article. Deleting folder {dest_path} ...")
    shutil.rmtree(dest_path)
    exit()


JSON_dict = {
    "url": path,
    "heading": headline,
    "subheading": subheading,
    "text": text,
    "author": author,
    "publish_date": date_iso8601,
    "tags": tags,
    "img_desc": valid_imgs,
    "id": id
}

with open(f"{dest_path}article.json", "w") as outfile:
    json.dump(JSON_dict, outfile, indent=4)
