import argparse
import random
import requests
from bs4 import BeautifulSoup
import os
from fake_useragent import UserAgent
import json
import shortuuid
from dateutil import parser, tz
import shutil


# update x.5
# transforming the scraper into a script so the link could be called from the main script
arg_parser = argparse.ArgumentParser(
    prog="help",
    description="This script takes one article link from abc news and scrapes the article \n"
                " and saves it as a folder with images and json file",
    epilog="If there are problems contact amirreza.alise@gmail.com"
)
arg_parser.add_argument("-l", "--Link", required=True,
                    help="The link to the article \n"
                         "eg. https://abcnews.go.com/US/110-million-alert-severe-dangerous-weather-holiday-weekend/story?id=100573102")
args = arg_parser.parse_args()

path = args.Link
#path = str(input("Enter the URL of the article: "))
# eg. https://abcnews.go.com/US/110-million-alert-severe-dangerous-weather-holiday-weekend/story?id=100573102


## update x.6
# building proxy list to avoid getting blocked
def LoadUpProxies():
    try:
        proxies = []
        url = 'https://sslproxies.org/'
        ua = UserAgent()
        header = {"User-Agent": ua.random}
        response = requests.get(url, headers=header)
        soup = BeautifulSoup(response.content, 'html.parser')
        soup2 = soup.find("div", class_="table-responsive fpl-list")
        soup3 = soup2.find("table", class_="table table-striped table-bordered").find("tbody").find_all("tr")[1:20]
        for i in soup3:
            try:
                ip = i.find_all('td')[0].string
                port = i.find_all('td')[1].string
                proxy = {'ip': ip, 'port': port}
                proxies.append(proxy)
            except:
                print('')
    except:
        proxies = [{'ip': '190.61.88.147', 'port': '8080'}, {'ip': '129.159.112.251', 'port': '3128'},
                   {'ip': '186.121.235.66', 'port': '8080'}, {'ip': '116.111.222.9', 'port': '23775'},
                   {'ip': '167.172.80.53', 'port': '3128'}, {'ip': '64.225.4.63', 'port': '9990'},
                   {'ip': '173.176.14.246', 'port': '3128'}, {'ip': '178.33.3.163', 'port': '8080'},
                   {'ip': '20.44.206.138', 'port': '80'}, {'ip': '129.153.157.63', 'port': '3128'},
                   {'ip': '158.160.56.149', 'port': '8080'}, {'ip': '186.121.235.222', 'port': '8080'},
                   {'ip': '3.141.13.89', 'port': '80'}, {'ip': '116.111.217.187', 'port': '23775'},
                   {'ip': '20.210.26.214', 'port': '3333'}, {'ip': '116.111.218.37', 'port': '23775'},
                   {'ip': '64.225.4.29', 'port': '9865'}, {'ip': '64.225.8.135', 'port': '9995'},
                   {'ip': '8.219.97.248', 'port': '80'}]
    return proxies


ua = UserAgent()
headers = {"User-Agent": ua.random}
try:
    # update x.6
    proxies = LoadUpProxies()
    proxy_index = random.randint(0, len(proxies) - 1)
    proxy_ip = proxies[proxy_index]['ip']
    proxy_port = proxies[proxy_index]['port']
    ###
    doc = requests.get(args.Link, headers=headers, proxies={"http": f"http://{proxy_ip}:{proxy_port}"}) # end of update x.6
    doc.encoding = "utf8"
    doc = BeautifulSoup(doc.text, "html.parser")
    pass

except:
    print("The URL or file name is invalid.")
    pass


def find_headline(doc):
    try:
        headline = doc.find("h1", class_="headline__text").string

    except:
        try:
            headline = doc.find("div", class_="headline").find("h1").string

        except:
            headline = ""

    return headline


# headline
headline = find_headline(doc)

id = shortuuid.uuid()
# path_name = slugify(headline, allow_unicode=True)
dest_path = f"{id}/"
if headline != "404":
    if not os.path.exists(dest_path):
        os.makedirs(dest_path)
    else:
        print("The article with this id already exists.")
        pass
    pass
else:
    print("error 404")


# subheading
# CNN does not have subheadings this is a placeholder
try:
    subheading = ""
    pass
except:
    try:
        subheading = ""
    except:
        subheading = ""
    pass


# author
def author_finder(doc):
    authors = []
    author_update = None
    try:
        try:
            authors = doc.find("div", class_="byline__names").find_all("a")

        except:
            # TODO live articles must be fixed later
            # byline-area
            author_area = doc.find("div", class_="headline")
            author_line = author_area.find("div", class_="sc-dnqmqq sc-hSdWYo fKJYAI")
            # using regex to find the author
            # eg. input <div class="sc-dnqmqq sc-hSdWYo fKJYAI"><p data-type="byline-area">By <a href="/profiles/kathleen-magramo">Kathleen Magramo</a> and Sana Noor Haq, CNN</p><div class="sc-dnqmqq hJIoKL"><span>Updated</span> 1009 GMT (1809 HKT) July 17, 2023</div></div>
            # output: ['Kathleen Magramo', 'and Sana Noor Haq']
            #regex = r"(?<=(<a.*>))(.+)(?=<\/a>)(?=<\/p>)"
            #author = re.findall(regex, str(author_line))

            try:
                authors1 = author_area.find_all("a").string
                authors2 = author_area.string
                author_update = "".join([authors1, authors2])

            except:
                authors = author_area.find_all("a")
                author_update = None

            pass

        if len(authors) == 1:
            author = [authors[0].find("span").string]
        else:
            try:
                author = [a.find("span").string for a in authors]
            except:
                author = [a.string for a in authors]
                pass
        pass

        if author_update:
            author = author_update

    except:
        author = []
        pass

    return author


author = author_finder(doc)


def date_finder(doc):
    try:
        try:
            date = doc.find("div", class_="timestamp").string
            # desired output: 2023-06-28T16:03:00.000Z
            # date = "
            #   Updated
            #         2:45 PM EDT, Fri July 14, 2023
            #     "
            # or
            # "
            #   Published
            #         10:04 AM EDT, Thu July 13, 2023
            #     "

            # making it parser format compatible
            try:
                date = date.split("Updated")[1]
            except:
                date = date.split("Published")[1]
                pass
        except:
            # output:
            '''
            5:39 a.m. ET, July 17, 2023
            '''

            date = doc.find("div", class_="sc-dnqmqq hJIoKL").string
            pass

        date = date.strip() # 2:45 PM EDT, Fri July 14, 2023
        print(date)
        datetime_object = parser.parse(date)  # 2023-07-11 00:00:00+03:00
        print("date_obj", datetime_object)
        #datetime_object = datetime_object.replace(tzinfo=None) + timedelta(hours=6)
        date_utc = datetime_object.astimezone(tz.gettz()).isoformat()  # 2023-07-11T00:00:00+03:00
        date_iso8601 = date_utc.split("+")[0] + "Z"  # 2023-07-11T03:00:00Z
        pass
    except:
        date_iso8601 = ""
        pass

    return date_iso8601


date_iso8601 = date_finder(doc)


# article body
def text_finder(doc):
    try:
        article_body = doc.find("div", class_="article__content").find_all("p")
        text = "".join([p.text for p in article_body])
        # source
        try:
            source = article_body.find_all("div")[0].find("cite").find("span")[1].string
            # adding source to the beginning of the text
            text = source + "\n" + text
        except:
            pass

    except:
        text = ""
        pass

    return text


text = text_finder(doc)


def image_finder(doc):
    valid_imgs = []

    try:
        header_image = None # pseudo-code placeholder
    except:
        header_image = None
        pass

    try:
        article = doc.find("article", class_="article").find("section", class_="body").find("main", class_="article__main")
        pictures = article.find_all("div", class_="image__container")


        # pseudo-code placeholder
        try:
            pics = doc.find("div", class_="js_starterpost").find("div", class_="js_post-content").find_all("figure")[
                   1:-1]
            pics_wraper = [p.find("div")[1:] for p in pics]
            for p in pics_wraper:
                if p not in pictures:
                    pictures.append(p)

        except:
            pass

    except:
        pictures = []
        pass

    # adding header image to first of the pictures list to be iterated
    if header_image is not None:
        pictures.insert(0, header_image)

    print(f"{len(pictures)} image-wrappers plus header found.")
    for picture_wraper in pictures:
        if picture_wraper == header_image:
            picture = picture_wraper.find("picture")
        else:
            try:
                picture = picture_wraper.find("picture")
                pass
            except:
                try:
                    picture = picture_wraper.find("div").find("picture")
                    pass
                except:
                    try:
                        picture = None
                        pass
                    except:
                        try:
                            picture = None
                            pass
                        except:
                            continue


        img = picture.find("img")
        img_src = img["src"]
        img_name = img_src.split("/")[-1].split("?")[0]

        try:
            img_data = requests.get(img_src, headers=headers).content
            print(f"Downloading {img_name}  ...")
            # we download the image with path img_src into a dist_path and adding .png extension
            # we have to convert images to jpg ,so we replace the existing extension with .png if there is one
            existing_format = img_name.split(".")[-1]
            if existing_format != "jpg":
                img_name = img_name.replace(f".{existing_format}", ".jpg")
                pass

            with open(f"{dest_path}{img_name}", "wb") as file:
                file.write(img_data)
                pass

            try:
                img_desc = None
                try:
                    # TODO: check if this works
                    fig_cap = picture_wraper.find_next_sibling("div", class_="image__metadata").find("div").find("span").string
                    img_desc = fig_cap
                except:
                    pass
                try:
                    img_desc = img["alt"]
                except:
                    pass

                if img_desc is not None:
                    image = {"filename": img_name, "description": img_desc}
                else:
                    image = {"filename": img_name}
                pass
            except:
                img_desc = None
                image = {"filename": img_name}
                pass

            valid_imgs.append(image)

            pass
        except:
            pass

    return valid_imgs


valid_imgs = image_finder(doc)


# tags
# Jezebel has no tags
tags = []

# update x.4
if len(valid_imgs) == 0:
    # stop the program and delete the dest_path folder if there is no image
    print(f"There are no images in thi article. Deleting folder {dest_path} ...")
    shutil.rmtree(dest_path)
    exit()


JSON_dict = {
    "url": path,
    "heading": headline,
    "subheading": subheading,
    "text": text,
    "author": author,
    "publish_date": date_iso8601,
    "tags": tags,
    "img_desc": valid_imgs,
    "id": id
}

with open(f"{dest_path}article.json", "w") as outfile:
    json.dump(JSON_dict, outfile, indent=4)
