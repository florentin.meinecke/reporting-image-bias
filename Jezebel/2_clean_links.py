import os
import numpy as np

def load_links(file):
    with open(file, 'r') as f:
        links = f.readlines()
    return links

# update x.7
prev_links = []
new_links = []
# load previous clean_links if exists
if os.path.exists("cache/clean_links.txt"):
    prev_links = load_links("cache/clean_links.txt")

if os.path.exists("cache/links.txt"):
    new_links = load_links("cache/links.txt")

print("number of new links:", len(new_links))
print("number of old links:", len(prev_links))

#cleaning the links file
with open("cache/clean_links.txt", "w+") as f:
    links = new_links
    temp = np.append(links, prev_links)
    clean_links = np.unique(temp)
    for link in clean_links:
        if link == "\n":
            continue
        if type(link) != str:
            link = str(link)


        if "[http" in link:
            link = link.replace("[http", "http")


        if "http://http" in link:
            link = link.replace("http://http", "http")


        if "link - http" in link:
            link = link.replace("link - http", "http")


        if "/author/" in link:
            continue

        if "/static/" in link:
            continue

        else:
            f.write(link)

print("new total unique links:", len(clean_links))
