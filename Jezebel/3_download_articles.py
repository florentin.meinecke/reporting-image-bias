import os
import time

with open("cache/clean_links.txt", "r") as file:
    article_links = file.readlines()
    pass

# update x.7
print(f"{len(article_links)} possible articles found in clean_links.txt to download, do you want to continue?")
while True:
    agreement = input("Enter Y to continue, N to stop, and M to enter manually:")
    if agreement == "Y":
        break
    if agreement == "N":
        exit()
    if agreement == "M":
        article_links = []
        while True:
            link = input(f"Enter a link, or enter C to continue with {len(article_links)} links:")
            if link == "C":
                break
            article_links.append(link)
        break
    else:
        print("invalid input.")
        continue



# trying the scraper on the links
# article extraction
for indx, link in enumerate(article_links):
    print(f"\nworking on the link number {indx}")
    print(f"link: {link}")
    os.system(f"python scraper.py -l {link}")
    time.sleep(0.1)
    pass
