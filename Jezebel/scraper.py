import argparse
import random
import shutil
from difflib import SequenceMatcher
import requests
import shortuuid
from bs4 import BeautifulSoup
import os
from fake_useragent import UserAgent
import json
from dateutil import parser, tz
import time


# update x.5
# transforming the scraper into a script so the link could be called from the main script
arg_parser = argparse.ArgumentParser(
    prog="help",
    description="This script takes one article link from abc news and scrapes the article \n"
                " and saves it as a folder with images and json file",
    epilog="If there are problems contact amirreza.alise@gmail.com"
)
arg_parser.add_argument("-l", "--Link", required=True,
                    help="The link to the article \n"
                         "eg. https://abcnews.go.com/US/110-million-alert-severe-dangerous-weather-holiday-weekend/story?id=100573102")
args = arg_parser.parse_args()

path = args.Link


#path = "https://jezebel.com/movies-bechdel-test-surprising-1850537372"
#path = str(input("Enter the URL of the article: "))
# eg. https://abcnews.go.com/US/110-million-alert-severe-dangerous-weather-holiday-weekend/story?id=100573102


## update x.6
# building proxy list to avoid getting blocked
def LoadUpProxies():
    try:
        proxies = []
        url = 'https://sslproxies.org/'
        ua = UserAgent()
        header = {"User-Agent": ua.random}
        response = requests.get(url, headers=header)
        soup = BeautifulSoup(response.content, 'html.parser')
        soup2 = soup.find("div", class_="table-responsive fpl-list")
        soup3 = soup2.find("table", class_="table table-striped table-bordered").find("tbody").find_all("tr")[1:20]
        for i in soup3:
            try:
                ip = i.find_all('td')[0].string
                port = i.find_all('td')[1].string
                proxy = {'ip': ip, 'port': port}
                proxies.append(proxy)
            except:
                print("Error loading proxies... using default proxies.")
    except:
        proxies = [{'ip': '190.61.88.147', 'port': '8080'}, {'ip': '129.159.112.251', 'port': '3128'},
                   {'ip': '186.121.235.66', 'port': '8080'}, {'ip': '116.111.222.9', 'port': '23775'},
                   {'ip': '167.172.80.53', 'port': '3128'}, {'ip': '64.225.4.63', 'port': '9990'},
                   {'ip': '173.176.14.246', 'port': '3128'}, {'ip': '178.33.3.163', 'port': '8080'},
                   {'ip': '20.44.206.138', 'port': '80'}, {'ip': '129.153.157.63', 'port': '3128'},
                   {'ip': '158.160.56.149', 'port': '8080'}, {'ip': '186.121.235.222', 'port': '8080'},
                   {'ip': '3.141.13.89', 'port': '80'}, {'ip': '116.111.217.187', 'port': '23775'},
                   {'ip': '20.210.26.214', 'port': '3333'}, {'ip': '116.111.218.37', 'port': '23775'},
                   {'ip': '64.225.4.29', 'port': '9865'}, {'ip': '64.225.8.135', 'port': '9995'},
                   {'ip': '8.219.97.248', 'port': '80'}]
    return proxies


ua = UserAgent()
headers = {"User-Agent": ua.random}
try:
    # update x.6
    proxies = LoadUpProxies()
    proxy_index = random.randint(0, len(proxies) - 1)
    proxy_ip = proxies[proxy_index]['ip']
    proxy_port = proxies[proxy_index]['port']
    ###
    doc = requests.get(path, headers=headers, proxies={"http": f"http://{proxy_ip}:{proxy_port}"}) # end of update x.6
    time.sleep(5)
    doc.encoding = "utf8"
    doc = BeautifulSoup(doc.text, "html.parser")
    pass

except:
    print("The URL or file name is invalid.")
    pass


def find_headline(doc):
    try:
        headline = doc.find("header").find_all("div")[0].find("h1").string

    except:
        try:
            headline = doc.find("header").find("h1").string
        except:
            headline = ""

    return headline


# headline
headline = find_headline(doc)

id = shortuuid.uuid()
# path_name = slugify(headline, allow_unicode=True)
dest_path = f"{id}/"
if headline != "404":
    if not os.path.exists(dest_path):
        os.makedirs(dest_path)
    else:
        print("The article with this id already exists.")
        pass
    pass
else:
    print("error 404")


# subheading
def find_subheading(doc):
    subheading = None
    try:
        subheading = doc.find("header").find("div").find_all("div")[2].text
    except:
        try:
            subheading = doc.find("h2", class_="sc-1xcxnn7-0 elFxuu js_regular-subhead").text
        except:
            subheading = doc.find("header").find_parent("div").find("h2").string

    if subheading == None:
        subheading = ""
        pass
    return subheading

subheading = find_subheading(doc)

# author
def author_finder(doc):
    try:
        authors = doc.find_all("div", class_="sc-1jc3ukb-4 gSRUuI")[0].find_all("div")

        if len(authors) == 1:
            author = [authors[0].string]
        else:
            author = [a.find("a").string for a in authors]
        pass

    except:
        try:
            author = [doc.find("div", class_="sc-1jc3ukb-4 gSRUuI").find("a").string]
        except:
            author = ""
            pass
    if author == None:
        author = []
        pass

    return author

def author_finder2(doc):
    try:
        authors = [doc.find_all("a", class_="sc-1out364-0 dPMosf permalink-bylineprop js_link").string]
    except:
        authors = ["Jezebel Staff"]
        pass
    return authors


author = author_finder(doc)
if len(author) == 0:
    author = author_finder2(doc)
if len(author) == 0:
    author = []
    pass




def date_finder(doc):
    # formatted according to ISO 8601 eg. 2021-05-29T00:00:00+10:00
    try:
        date = doc.find_all("div", class_="sc-1jc3ukb-2 fHkffo")[0].find("time")["datetime"]
    except:
        date = doc.find("div", class_="sc-1jc3ukb-2 gpatta").find("time")["datetime"]

    # desired output: 2023-06-28T16:03:00.000Z
    datetime_object = parser.parse(date)  # 2023-07-11 00:00:00
    date_utc = datetime_object.astimezone(tz.gettz()).isoformat()  # 2023-07-11T00:00:00+03:00
    date_iso8601 = date_utc.split("+")[0] + "Z"  # 2023-07-11T03:00:00Z
    pass

    if date_iso8601 == None:
        date_iso8601 = ""
        pass

    return date_iso8601


date_iso8601 = date_finder(doc)


# article body
def text_finder(doc):
    try:
        article_body = doc.find_all("div", class_="sc-r43lxo-1 cwnrYD")[0].find_all("p")
        text = "".join([p.text for p in article_body])

    except:
        text = ""
        pass

    return text


text = text_finder(doc)


def image_finder(doc):
    valid_imgs = []

    try:
        header_image = doc.find("div", class_="sc-gkv9lo-2 icqIZs")
    except:
        header_image = None
        pass

    try:
        pics = doc.find_all("picture", class_="sc-epkw7d-0 diKDHf")
        pictures = [p.find_parent("div", class_="sc-1eow4w5-2 fDJNBs has-data img-wrapper") for p in pics]


        try:
            pics = doc.find_all("picture", class_="sc-epkw7d-0 jkuixZ")
            pics_wraper = [p.find_parent("div", class_="sc-1eow4w5-2 fDJNBs has-data img-wrapper") for p in pics]
            for p in pics_wraper:
                if p not in pictures:
                    pictures.append(p)
        except:
            pass


        try:
            pics = doc.find("div", class_="js_starterpost").find("div", class_="js_post-content").find_all("figure")[
                   1:-1]
            pics_wraper = [p.find("div")[1:] for p in pics]
            for p in pics_wraper:
                if p not in pictures:
                    pictures.append(p)

        except:
            pass

        try:
            pics_wraper = doc.find_all("div", class_="sc-1eow4w5-2 fDJNBs has-data img-wrapper")
            if pics_wraper not in pictures:
                pictures.append(pics_wraper)
        except:
            pass

        try:
            pics_wraper = doc.find_all("div", class_="sc-1eow4w5-2 fDJNBs has-data img-wrapper")[1:-1]
            for p in pics_wraper:
                if p not in pictures:
                    pictures.append(p)
        except:
            pass


        try:
            pics_wraper = doc.find_all("div", class_="img-wrapper")[:-1]
            for p in pics_wraper:
                if p not in pictures:
                    pictures.append(p)
        except:
            pass

    except:
        pictures = []
        pass

    # adding header image to first of the pictures list to be iterated
    if header_image is not None:
        pictures.insert(0, header_image)

    print(f"{len(pictures)} image-wrappers plus header found.")
    for picture_wraper in pictures:
        if picture_wraper == header_image:
            picture = picture_wraper.find("picture")
        else:
            try:
                picture = picture_wraper.find("span").find_all("div")[1].find("picture")
                pass
            except:
                try:
                    # TODO not working for article image in this type of articles
                    # eg. https://jezebel.com/midwife-abortion-care-1850631246
                    picture = picture_wraper.find("span").find_all("div")[1].find("picture")
                    pass
                except:
                    try:
                        picture = picture_wraper.find_all("div")[0].find("picture")
                        pass
                    except:
                        try:
                            picture = picture_wraper.find_parent("figure").find("picture")
                            pass
                        except:
                            continue


        img = picture.find("img")
        img_src = img["src"]
        img_name = img_src.split("/")[-1].split("?")[0]

        # update x.7
        # removing extra dots in image name
        try:
            dots = img_name.count(".")
            format = img_name.split(".")[-1]
            name = img_name.split(".")[:-1]
            if dots > 1:
                img_name = "".join(name) + "." + format
                pass
        except:
            pass

        try:
            img_data = requests.get(img_src, headers=headers).content
            print(f"Downloading {img_name}... saving as jpg")

            # update x.7
            def save_duplicate(img_name, count=0):

                if os.path.exists(f"{dest_path}{img_name}"):
                    count += 1
                    new_img_name = f"{img_name}({count}).jpg"
                    save_duplicate(new_img_name, count)

                if count >= 1:
                    new_img_name = f"{img_name}({count}).jpg"
                else:
                    new_img_name = img_name

                return new_img_name  # end of update x.7

            # update x.4 fixing JPG formatting
            existing_format = img_name.split(".")[-1]
            if existing_format != "jpg":
                img_name = img_name.replace(f".{existing_format}", ".jpg")
                pass

            img_name = save_duplicate(img_name)  # end of update x.4


            with open(f"{dest_path}{img_name}", "wb") as file:
                file.write(img_data)
                pass

            try:
                img_desc = img["alt"]
                if img_desc is None or img_desc == "":
                    fig_cap = picture_wraper.find("div").find("figcaption").string
                    img_desc = fig_cap

                image = {"filename": img_name, "description": img_desc}
            except:
                img_desc = None
                image = {"filename": img_name}
                pass

            valid_imgs.append(image)

            pass
        except:
            pass

    return valid_imgs


valid_imgs = image_finder(doc)


# tags
# Jezebel has no tags
tags = []

# update x.4
if len(valid_imgs) == 0:
    # stop the program and delete the dest_path folder if there is no image
    print(f"There are no images in this article. Deleting folder {dest_path} ...")
    shutil.rmtree(dest_path)
    exit() # end of update x.4

JSON_dict = {
    "url": path,
    "heading": headline,
    "subheading": subheading,
    "text": text,
    "author": author,
    "publish_date": date_iso8601,
    "tags": tags,
    "img_desc": valid_imgs,
    "id": id
}

with open(f"{dest_path}article.json", "w") as outfile:
    json.dump(JSON_dict, outfile, indent=4)

print(f"##############################################")
