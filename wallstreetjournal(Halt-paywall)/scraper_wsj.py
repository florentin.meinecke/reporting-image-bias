import requests
from bs4 import BeautifulSoup
import os
from fake_useragent import UserAgent
import json
from slugify import slugify

path = str(input("Enter the URL of the news or the file name that is in the directory: "))

# eg. https://www.wsj.com/articles/for-the-u-s-and-nato-a-weakened-russia-is-more-of-a-wild-card-edf6fbe2

# path = "HTMLDB/For the U.S. and NATO, a Weakened Russia Is More of a Wild Card - WSJ.html"


# HTML_file = open(path, encoding="utf8")
# doc = BeautifulSoup(HTML_file)
# print(doc.prettify())

'''
# try to open the URL
url = "https://www.wsj.com/articles/u-s-home-prices-posted-first-annual-decline-since-2012-in-april-a5d0bbdc?mod=hp_lead_pos6"
doc = requests.get(url)
doc.encoding = "utf8"
doc = BeautifulSoup(doc.text, "html.parser")
print(doc.prettify())


'''

ua = UserAgent()
headers = {"User-Agent": ua.random}
try:
    doc = requests.get(path, headers=headers)
    doc.encoding = "utf8"
    doc = BeautifulSoup(doc.text, "html.parser")
except:
    # open the file in folder HTMLDB
    with open(path, encoding="utf8") as file:
        doc = BeautifulSoup(file, "html.parser")
        pass
finally:
    print("The URL or file name is invalid.")
    pass

# doc = BeautifulSoup(result.text, "html.parser")

# find and print the headline of the article
# it's the last object with class StyledHeadline

try:
    StyledHeadline_class = doc.find_all(class_="css-1lvqw7f-StyledHeadline")
    headline = StyledHeadline_class[-1].string
    pass
except:
    headline = doc.find("h1").string
    pass

path_name = slugify(headline, allow_unicode=True)
dest_path = f"{path_name}/"
if headline != "404":
    if not os.path.exists(dest_path):
        os.makedirs(dest_path)
    else:
        print("The headline already exists.")
        pass
    pass
else:
    print("error 404")

# saving the headline in a text file
# with open(f"{headline}/headline.txt", "w") as file:
#    file.write(headline)
#    pass

# article heading
# article_heading = doc.find("h1").string
# print(article_heading)


# now we find the images in the article
# we find all the <picture> tags and their <img> subtags
# then we save the image in HTMLDB folder

valid_imgs = {}
valid_count = -1
pictures = doc.find_all("picture")
for picture in pictures:
    img = picture.find("img")
    img_src = img["src"]
    img_name = img_src.split("/")[-1].split("?")[0]

    try:
        img_data = requests.get(img_src, headers=headers).content
        print(f"Downloading {img_name}  ...")
        # we download the image with path img_src into a dist_path and adding .png extension
        with open(f"{dest_path}{img_name}.png", "wb") as file:
            file.write(img_data)

        valid_count += 1
        img_desc = img["alt"]

        if img_desc is None:
            try:
                fig_cap = doc.find_all("figcaption")
                cap = fig_cap[valid_count].find("span")
                img_desc = cap.string

            except:
                img_desc = " "

        valid_imgs[img_name] = img_desc

        pass
    except:
        pass

print(valid_imgs)

dictionary = {
    "link": path,
    "heading": headline,
    "subheading": "",
    "text": "",
    "author": "",
    "date": "",
    "tags": "",
    "img_desc": valid_imgs
}

with open(f"{dest_path}article.json", "w") as outfile:
    json.dump(dictionary, outfile)
