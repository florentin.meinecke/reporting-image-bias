import argparse
import random
import shutil
from difflib import SequenceMatcher
import requests
import shortuuid
from bs4 import BeautifulSoup
import os
from fake_useragent import UserAgent
import json
from dateutil import parser, tz
import time


# update x.5
# transforming the scraper into a script so the link could be called from the main script
arg_parser = argparse.ArgumentParser(
    prog="help",
    description="This script takes one article link from abc news and scrapes the article \n"
                " and saves it as a folder with images and json file",
    epilog="If there are problems contact amirreza.alise@gmail.com"
)
arg_parser.add_argument("-l", "--Link", required=True,
                    help="The link to the article \n"
                         "eg. https://abcnews.go.com/US/110-million-alert-severe-dangerous-weather-holiday-weekend/story?id=100573102")
args = arg_parser.parse_args()

path = args.Link


#path = "https://www.huffpost.com/entry/ron-desantis-campaign-staff-cuts_n_64c02d97e4b09a9296947d83"
#path = str(input("Enter the URL of the article: "))
# eg. https://abcnews.go.com/US/110-million-alert-severe-dangerous-weather-holiday-weekend/story?id=100573102


## update x.6
# building proxy list to avoid getting blocked
def LoadUpProxies():
    try:
        proxies = []
        url = 'https://sslproxies.org/'
        ua = UserAgent()
        header = {"User-Agent": ua.random}
        response = requests.get(url, headers=header)
        soup = BeautifulSoup(response.content, 'html.parser')
        soup2 = soup.find("div", class_="table-responsive fpl-list")
        soup3 = soup2.find("table", class_="table table-striped table-bordered").find("tbody").find_all("tr")[1:20]
        for i in soup3:
            try:
                ip = i.find_all('td')[0].string
                port = i.find_all('td')[1].string
                proxy = {'ip': ip, 'port': port}
                proxies.append(proxy)
            except:
                print('')
    except:
        proxies = [{'ip': '190.61.88.147', 'port': '8080'}, {'ip': '129.159.112.251', 'port': '3128'},
                   {'ip': '186.121.235.66', 'port': '8080'}, {'ip': '116.111.222.9', 'port': '23775'},
                   {'ip': '167.172.80.53', 'port': '3128'}, {'ip': '64.225.4.63', 'port': '9990'},
                   {'ip': '173.176.14.246', 'port': '3128'}, {'ip': '178.33.3.163', 'port': '8080'},
                   {'ip': '20.44.206.138', 'port': '80'}, {'ip': '129.153.157.63', 'port': '3128'},
                   {'ip': '158.160.56.149', 'port': '8080'}, {'ip': '186.121.235.222', 'port': '8080'},
                   {'ip': '3.141.13.89', 'port': '80'}, {'ip': '116.111.217.187', 'port': '23775'},
                   {'ip': '20.210.26.214', 'port': '3333'}, {'ip': '116.111.218.37', 'port': '23775'},
                   {'ip': '64.225.4.29', 'port': '9865'}, {'ip': '64.225.8.135', 'port': '9995'},
                   {'ip': '8.219.97.248', 'port': '80'}]
    return proxies


ua = UserAgent()
headers = {"User-Agent": ua.random}
try:
    # update x.6
    proxies = LoadUpProxies()
    proxy_index = random.randint(0, len(proxies) - 1)
    proxy_ip = proxies[proxy_index]['ip']
    proxy_port = proxies[proxy_index]['port']
    ###
    doc = requests.get(path, headers=headers, proxies={"http": f"http://{proxy_ip}:{proxy_port}"}) # end of update x.6
    time.sleep(1)
    doc.encoding = "utf8"
    doc = BeautifulSoup(doc.text, "html.parser")
    pass

except:
    print("The URL or file name is invalid.")
    pass

# headline
try:
    headline = doc.find("h1", class_="headline").string
    pass
except:
    headline = ""
    pass

# subheading
try:
    subheading = doc.find("div", class_="dek").string

except:
    subheading = ""
    pass

# author
def author_finder(doc):
    try:
        div = doc.find("div", class_="entry__byline__author")
        authors = div.find_all("a")
        if len(authors) == 1:
            author = [authors[0].text]
        else:
            # all except the last one which is always FOX_news
            author = [a.text for a in authors[:-1]]
        pass
    except:
        try:
            author = [] # placeholder

            pass
        except:
            try:
                author = [] # placeholder
                pass

            except:
                author = []
                pass

    return author


author = author_finder(doc)

# date
# date should be in iso8601 format and in UTC
try:
    date = doc.find("div", class_="timestamp").find("time")["datetime"] # eg.  2023-07-23
    date_obj = parser.parse(date)

    date_utc = date_obj.astimezone(tz.gettz("UTC")).isoformat() # eg. 2023-07-01 14:14:00+00:00
    # converting to iso8601 format

    date_iso8601 = date_utc.split("+")[0] + "Z" # eg. 2023-07-01T14:14:00Z
except:
    date_iso8601 = ""
    pass

# article body
try:
    divs = doc.find_all("div", class_="primary-cli")
    paragraphs = [div.find_all("p") for div in divs]
    ts = []
    for p in paragraphs:
        try:
            t = p[0].string
            if t == None:
                t = p[0].text
            pass
        except:
            t = ""
            pass

        ts.append(t)
        continue
    text = "".join(ts)
except:
    text = ""
    pass

id = shortuuid.uuid()
#path_name = slugify(headline, allow_unicode=True)
dest_path = f"{id}/"
if headline != "404":
    if not os.path.exists(dest_path):
        os.makedirs(dest_path)
    else:
        print("The article with this id already exists.")
        pass
    pass
else:
    print("error 404")

valid_imgs = []
figures = doc.find_all("figure")
for figure in figures:
    picture = figure.find("div", class_="img-sized").find("picture")
    img = picture.find("img")
    img_src = img["src"]
    img_name = img_src.split("/")[-1].split("?")[0]

    # update x.7
    # removing extra dots in image name
    try:
        dots = img_name.count(".")
        format = img_name.split(".")[-1]
        name = img_name.split(".")[:-1]
        if dots > 1:
            img_name = "".join(name) + "." + format
            pass
    except:
        pass

    try:
        img_data = requests.get(img_src, headers=headers).content
        print(f"Downloading {img_name}... saving as jpg")
        # if the image exists, the new one will be saved with a different name

        # update x.7
        def save_duplicate(img_name, count=0):

            if os.path.exists(f"{dest_path}{img_name}"):
                count += 1
                new_img_name = f"{img_name}({count}).jpg"
                save_duplicate(new_img_name, count)

            if count >= 1:
                new_img_name = f"{img_name}({count}).jpg"
            else:
                new_img_name = img_name

            return new_img_name # end of update x.7


        # update x.4 fixing JPG formatting
        existing_format = img_name.split(".")[-1]
        if existing_format != "jpg":
            img_name = img_name.replace(f".{existing_format}", ".jpg")
            pass

        img_name = save_duplicate(img_name) # end of update x.4



        try:
            img_desc = None
            try:
                fig_cap = picture.find("div", class_="cli-image__source-wrapper").find("figcaption")
                img_desc = fig_cap.string

            except:
                pass

            if img_desc is None or img_desc == "":
                img_desc = img["alt"]

        except:
            img_desc = None

        if img_desc is not None or img_desc != "":
            image = {"filename": img_name, "description": img_desc}
        else:
            image = {"filename": img_name}

        # if 70% of the img_desc is similar to the author, it will be removed
        def similar(a, b):
            return SequenceMatcher(None, a, b).ratio()

        # update x.7
        for auth in author:
            ratio = similar(auth, img_desc)
            if ratio >= 0.7:
                percent = round(ratio * 100, 2)
                print(f"Skipping {img_name} because its description is {percent} percent "
                      f"similar to authors.")

                continue
            else:
                with open(f"{dest_path}{img_name}", "wb") as file:
                    file.write(img_data)

                valid_imgs.append(image)

    except:
        pass

# tags
try:
    tags_div = doc.find("section", class_="entry__tags").find_all("a")
    tags = [tag.string for tag in tags_div]

except:
    tags = []
    pass

# update x.4
if len(valid_imgs) == 0:
    # stop the program and delete the dest_path folder if there is no image
    print(f"There are no images in this article. Deleting folder {dest_path} ...")
    shutil.rmtree(dest_path)
    exit()

JSON_dict = {
    "url": path,
    "heading": headline,
    "subheading": subheading,
    "text": text,
    "author": author,
    "publish_date": date_iso8601,
    "tags": tags,
    "img_desc": valid_imgs,
    "id": id
}

with open(f"{dest_path}article.json", "w") as outfile:
    json.dump(JSON_dict, outfile, indent=4)

