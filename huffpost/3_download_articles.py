import os

with open("cache/clean_links.txt", "r") as file:
    article_links = file.readlines()
    pass

print(f"{len(article_links)} possible articles to download.")

# update x.8
if not os.path.exists("cache/archive.txt"):
    with open("cache/archive.txt", "w") as file:
        file.write("")
with open("cache/archive.txt", "r") as file:
    indx = file.readlines()


for link in article_links:
    if link in indx:
        print(f"link {link} is already downloaded.")
        article_links.remove(link)
    else:
        with open("cache/archive.txt", "a") as file:
            file.write(link) # end of update x.8

# update x.7.2
def download_articles(start, target, step_size=50, indx=0):

    if indx == 0:
        indx = start

    end = start + step_size
    if end > target:
        end = target

    range1 = range(start, end)
    print(f"working on the range {range1}")
    '''
    agreement = input(f"Enter Y to continue with {range1} and N to stop: ")
    if agreement == "Y":
        pass
    if agreement == "N":
        exit()
    '''

    # trying the scraper on the links
    # article extraction
    for link in article_links[start:end]:
        print(f"link {link}")
        print(f"working on the link number {indx}")
        os.system(f"python scraper.py -l {link}")
        #time.sleep(0.1)
        indx += 1
        pass

    start = end-1
    state = False
    if start < target:
        state = True
    while state:
        download_articles(start, target, step_size=step_size, indx=indx)
        pass


download_articles(0, len(article_links), step_size=50, indx=0)
