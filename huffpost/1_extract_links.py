import multiprocessing
import sys
import numpy as np
import requests
import os
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
import time
from joblib import Parallel, delayed
from functools import partial
from random import randrange


start_link = 'https://www.huffpost.com/'
shared_part = 'https://www.huffpost.com'

# update x.6
# building proxy list to prevent getting blocked
def LoadUpProxies():
    try:
        proxies = []
        url = 'https://sslproxies.org/'
        ua = UserAgent()
        header = {"User-Agent": ua.random}
        response = requests.get(url, headers=header)
        soup = BeautifulSoup(response.content, 'html.parser')
        soup2 = soup.find("div", class_="table-responsive fpl-list")
        soup3 = soup2.find("table", class_="table table-striped table-bordered").find("tbody").find_all("tr")[1:20]
        for i in soup3:
            try:
                ip = i.find_all('td')[0].string
                port = i.find_all('td')[1].string
                proxy = {'ip': ip, 'port': port}
                proxies.append(proxy)
            except:
                print('')
    except:
        proxies = [{'ip': '190.61.88.147', 'port': '8080'}, {'ip': '129.159.112.251', 'port': '3128'},
                   {'ip': '186.121.235.66', 'port': '8080'}, {'ip': '116.111.222.9', 'port': '23775'},
                   {'ip': '167.172.80.53', 'port': '3128'}, {'ip': '64.225.4.63', 'port': '9990'},
                   {'ip': '173.176.14.246', 'port': '3128'}, {'ip': '178.33.3.163', 'port': '8080'},
                   {'ip': '20.44.206.138', 'port': '80'}, {'ip': '129.153.157.63', 'port': '3128'},
                   {'ip': '158.160.56.149', 'port': '8080'}, {'ip': '186.121.235.222', 'port': '8080'},
                   {'ip': '3.141.13.89', 'port': '80'}, {'ip': '116.111.217.187', 'port': '23775'},
                   {'ip': '20.210.26.214', 'port': '3333'}, {'ip': '116.111.218.37', 'port': '23775'},
                   {'ip': '64.225.4.29', 'port': '9865'}, {'ip': '64.225.8.135', 'port': '9995'},
                   {'ip': '8.219.97.248', 'port': '80'}]
    return proxies


proxies = LoadUpProxies()
# print(proxies)


def update_file(new_links):
    print('number of new links:', len(new_links))
    with open('cache/links.txt', 'r') as f:
        old_links = f.readlines()
        print('number of old links:', len(old_links))
    links = np.append(old_links, new_links)
    un_links = np.unique(links)
    print('new total unique links:', len(un_links))

    with open('cache/links.txt', 'w') as f:
        for link in un_links:
            f.write(link + '\n')
    return


def load_links(file):
    with open(file, 'r') as f:
        links = f.readlines()
    return links


###########################################################

"""
the point of this script is to extract all the links from the cnn website home page and run the scraper on them
eg. https://edition.cnn.com

"""

if not os.path.exists("cache"):
    os.makedirs("cache")

article_links = []
# get the links from the home page
ua = UserAgent()
headers = {"User-Agent": ua.random}
doc = requests.get(start_link, headers=headers, proxies=proxies[randrange(len(proxies))])
doc = BeautifulSoup(doc.text, "html.parser")
links = doc.find_all("a")
for link in links:
    if link.has_attr("href"):
        link = link["href"]
        try:
            if link.split("/")[-1] == "index.html":
                pass
            # not starting with http or https
            if "http" not in link[0:4]:
                link = shared_part + link
        except:
            pass
        if shared_part in link:
            article_links.append(link)


# save the links
article_links = np.unique(article_links)
with open('cache/links.txt', 'w') as f:
    for link in article_links:
        f.write(link + '\n')
print('number of initial links:', len(article_links))

###########################################################



def second_level_links(input_links, links_per_link=100):
    # 2nd level links x * links_per_link = number of potential links
    article_links_2nd = []
    #print(f"Digging in {len(input_links)} links. \n Query size: {links_per_link} Please wait...")
    for indx, link in enumerate(input_links):
        try:
            # using random proxy
            proxy_index = randrange(len(proxies))
            proxy_ip = proxies[proxy_index]['ip']
            proxy_port = proxies[proxy_index]['port']

            ua = UserAgent()
            headers = {"User-Agent": ua.random}
            doc = requests.get(link, headers=headers, proxies={"http": f"http://{proxy_ip}:{proxy_port}"})
            doc.encoding = "utf8"
            # time.sleep(5)
            doc = BeautifulSoup(doc.text, "html.parser")
            page_links = doc.find_all("a")[0:links_per_link]
            for link in page_links:
                if link.has_attr("href"):
                    link = link["href"]
                    try:
                        if link.split("/")[-1] == "index.html":
                            pass
                        # not starting with http or https
                        if "http" not in link[0:4]:
                            link = shared_part + link
                    except:
                        pass
                    if shared_part in link:
                        article_links_2nd.append(link)

        except:
            error = sys.exc_info()[0]
            #print(f"link {url} error: {error}")

            continue

    return article_links_2nd


new_article_links = second_level_links(article_links, links_per_link=100)
print('number of 2nd level links:', len(new_article_links))
update_file(new_article_links)

# recursive digging
# multiprocessing
def recursive_digging(input_links_file, target_links=1000, round_dig=1, links_per_link=100, max_rounds=10, max_sampling=1000):
    if round_dig == max_rounds:
        print("Max rounds reached. Stopping the recursive digging...")
        return

    file = load_links(input_links_file)
    input_links = np.random.choice(file, max_sampling)
    print(f"Round {round_dig} of recursive digging")
    round_dig += 1
    bath_size = 1
    if len(input_links) > 100:
        #bath_size = len(input_links) // 10  # 10% of the links
        bath_size = 16
        if len(input_links) > 10000:
            bath_size = 256
    batches = np.array_split(input_links, bath_size)
    print("Number of batches: ", len(batches), "batch size: ", len(batches[0]), "total links: ", len(file), "target links: ", target_links)
    time.sleep(5)

    if len(batches) > 1:
        # Multiprocessing
        num_cores = multiprocessing.cpu_count()
        second_level_links_ = partial(second_level_links, links_per_link=links_per_link)
        output_links = Parallel(n_jobs=num_cores, prefer="threads")(delayed(second_level_links_)(i) for i in batches)
        output_links = np.concatenate(output_links)
        #output_links = np.unique(output_links)
        update_file(output_links)

    else:
        output_links = second_level_links(input_links, links_per_link=links_per_link)  # number of 1st level links *
        # links_per_link = number of potential links
        update_file(output_links)

    # update x.7
    new_file = load_links("cache/links.txt")
    progress = len(new_file) / len(file)
    if round_dig > 8 and progress < 1.05:
        print(f"Progress is too low {(progress - 1) *100} percent. Stopping the recursive digging...")
        return

    if len(new_file) < target_links:
        recursive_digging("cache/links.txt", target_links=target_links, round_dig=round_dig, max_rounds=max_rounds)
    if len(new_file) > target_links:
        print(f"Target links reached. Stopping the recursive digging...")

    return


recursive_digging("cache/links.txt", target_links=50000, links_per_link=300, max_rounds=50, max_sampling=1600)

print("number of links found by digging", len(load_links("cache/links.txt")))
