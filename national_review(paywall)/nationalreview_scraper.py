import requests
from bs4 import BeautifulSoup
import os
from fake_useragent import UserAgent
import json
from slugify import slugify

path = str(input("Enter the URL of the article: "))
# eg. https://www.foxnews.com/world/mexican-family-6-year-old-boy-fell-amusement-park-zipline-file-lawsuit

ua = UserAgent()
headers = {"User-Agent": ua.random}
try:
    doc = requests.get(path, headers=headers)
    doc.encoding = "utf8"
    doc = BeautifulSoup(doc.text, "html.parser")
    pass

except:
    print("The URL or file name is invalid.")
    pass

# headline
try:
    headline = doc.find("h1", class_="article-header__title").string
    pass
except:
    headline = "headline not found"
    pass

# subheading
try:
    subheading = doc.find("h2", class_="sub-headline").string
    pass
except:
    subheading = "subheading not found"
    pass

# author
def author_finder(doc):
    try:
        spans = doc.find("div", class_="article-author__names").find_all("div", class_="article-author__name--container").find
        spans = [s.find_all("a")[0] for s in spans]
        authors = [a for a in spans]
        if len(authors) == 1:
            author = authors[0].string
        else:
            author = [a.string for a in authors]
        pass

    except:
        try:
            author = doc.find("div", class_="post-author").find_all("a")[1].string
            pass
        except:
            author = "author not found"
            pass

    return author


author = author_finder(doc)

# date
try:
    date = doc.find("span", class_="post-date").string
    pass
except:
    date = "date not found"
    pass

# article body
try:
    article_body = doc.find("div", class_="body").find("div", class_="body-description").find_all("p")
    text = "".join([p.text for p in article_body])
    pass
except:
    text = "article text not found"
    pass

path_name = slugify(headline, allow_unicode=True)
dest_path = f"{path_name}/"
if headline != "404":
    if not os.path.exists(dest_path):
        os.makedirs(dest_path)
    else:
        print("The headline already exists.")
        pass
    pass
else:
    print("error 404")

valid_imgs = {}
valid_count = -1
pictures = doc.find_all("picture")
for picture in pictures:
    img = picture.find("img")
    img_src = img["src"]
    img_name = img_src.split("/")[-1].split("?")[0]

    try:
        img_data = requests.get(img_src, headers=headers).content
        print(f"Downloading {img_name}  ...")
        # we download the image with path img_src into a dist_path and adding .png extension
        with open(f"{dest_path}{img_name}", "wb") as file:
            file.write(img_data)

        valid_count += 1
        img_desc = img["alt"]

        if img_desc is None:
            try:
                fig_cap = doc.find_all("figcaption")
                cap = fig_cap[valid_count].find("span")
                img_desc = cap.string

            except:
                img_desc = " "
                pass

        valid_imgs[img_name] = img_desc

        pass
    except:
        pass

# tags
try:
    tags = doc.find("div", class_="article-tags").find_all("a")
    pass
except:
    tags = "tags not found"
    pass

print(valid_imgs)

JSON_dict = {
    "url": path,
    "heading": headline,
    "subheading": subheading,
    "text": text,
    "author": author,
    "publish_date": date,
    "tags": tags,
    "img_desc": valid_imgs
}

with open(f"{dest_path}article.json", "w") as outfile:
    json.dump(JSON_dict, outfile, indent=4)

