import argparse
import random
import shutil
import requests
import shortuuid
from bs4 import BeautifulSoup
import os
from fake_useragent import UserAgent
import json
from dateutil import parser, tz



# update x.5
# transforming the scraper into a script so the link could be called from the main script
arg_parser = argparse.ArgumentParser(
    prog="help",
    description="This script takes one article link from abc news and scrapes the article \n"
                " and saves it as a folder with images and json file",
    epilog="If there are problems contact amirreza.alise@gmail.com"
)
arg_parser.add_argument("-l", "--Link", required=True,
                    help="The link to the article \n"
                         "eg. https://abcnews.go.com/US/110-million-alert-severe-dangerous-weather-holiday-weekend/story?id=100573102")
args = arg_parser.parse_args()

path = args.Link

#path = "https://www.alternet.org/2019/05/new-report-reveals-how-trumps-anti-science-views-are-warping-governments-vital-research-on-climate-change"

print(path)

#path = str(input("Enter the URL of the article: "))
# eg. https://abcnews.go.com/US/110-million-alert-severe-dangerous-weather-holiday-weekend/story?id=100573102



## update x.6
# building proxy list to avoid getting blocked
def LoadUpProxies():
    try:
        proxies = []
        url = 'https://sslproxies.org/'
        ua = UserAgent()
        header = {"User-Agent": ua.random}
        response = requests.get(url, headers=header)
        soup = BeautifulSoup(response.content, 'html.parser')
        soup2 = soup.find("div", class_="table-responsive fpl-list")
        soup3 = soup2.find("table", class_="table table-striped table-bordered").find("tbody").find_all("tr")[1:20]
        for i in soup3:
            try:
                ip = i.find_all('td')[0].string
                port = i.find_all('td')[1].string
                proxy = {'ip': ip, 'port': port}
                proxies.append(proxy)
            except:
                print('')
    except:
        proxies = [{'ip': '190.61.88.147', 'port': '8080'}, {'ip': '129.159.112.251', 'port': '3128'},
                   {'ip': '186.121.235.66', 'port': '8080'}, {'ip': '116.111.222.9', 'port': '23775'},
                   {'ip': '167.172.80.53', 'port': '3128'}, {'ip': '64.225.4.63', 'port': '9990'},
                   {'ip': '173.176.14.246', 'port': '3128'}, {'ip': '178.33.3.163', 'port': '8080'},
                   {'ip': '20.44.206.138', 'port': '80'}, {'ip': '129.153.157.63', 'port': '3128'},
                   {'ip': '158.160.56.149', 'port': '8080'}, {'ip': '186.121.235.222', 'port': '8080'},
                   {'ip': '3.141.13.89', 'port': '80'}, {'ip': '116.111.217.187', 'port': '23775'},
                   {'ip': '20.210.26.214', 'port': '3333'}, {'ip': '116.111.218.37', 'port': '23775'},
                   {'ip': '64.225.4.29', 'port': '9865'}, {'ip': '64.225.8.135', 'port': '9995'},
                   {'ip': '8.219.97.248', 'port': '80'}]
    return proxies

#path = str(input("Enter the URL of the article: "))
# eg. https://www.alternet.org/zelenskyy-happy-fourth-of-july/


ua = UserAgent()
headers = {"User-Agent": ua.random}
try:
    # update x.6
    proxies = LoadUpProxies()
    proxy_index = random.randint(0, len(proxies) - 1)
    proxy_ip = proxies[proxy_index]['ip']
    proxy_port = proxies[proxy_index]['port']
    ###
    doc = requests.get(path, headers=headers, proxies={"http": f"http://{proxy_ip}:{proxy_port}"})  # update x.6
    doc.encoding = "utf8"
    doc = BeautifulSoup(doc.text, "html.parser")
    pass

except:
    doc = BeautifulSoup("404", "html.parser")
    print("The URL or file name is invalid.")
    pass

# headline
try:
    headline = doc.find("h1", class_="headline h1").string
    pass
except:
    headline = ""
    pass

# subheading
try:
    subheading = doc.find("h2", class_="sub-headline").string
    pass
except:
    subheading = ""
    pass

# author
def author_finder(doc):
    try:
        spans = doc.find("div", class_="post-author-list").find_all("div", class_="post-author")
        spans = [s.find_all("a")[1] for s in spans]
        authors = [a for a in spans]
        if len(authors) == 1:
            author = [authors[0].string]
        else:
            author = [a.string for a in authors]
        pass

    except:
        try:
            author = doc.find("div", class_="post-author").find_all("a")[1].string
            pass
        except:
            author = ""
            pass

    return author


author = author_finder(doc)

# date
# date format should be in ISO 8601 and UTC time zone
try:
    # example output: 2023-07-08T11:07:00Z
    date = doc.find("span", class_="post-date").string # eg. "May 28, 2021"
    date_obj = parser.parse(date)

    # converting to UTC
    date_utc = date_obj.astimezone(tz.tzutc()) # eg. "2021-05-28 00:00:00+00:00"
    # converting to ISO 8601 eg. "2021-05-28T00:00:00Z"
    date_iso8601 = str(date_utc.isoformat()).split("+")[0] + "Z"

    '''
    date = doc.find("span", class_="post-date").string # eg. "May 28, 2021"
    date_new = date.split(", ")[1] + " " + date.split(", ")[0] # eg. "2021 May 28"
    date_new2 = date.replace(" ", "-") # eg. "2021-May-28"
    date_new3 = date_new2.split("-")[1].replace(",", "") + " " + date_new2.split("-")[0] + " " + date_new2.split("-")[2] # should be day month year
    date_iso = datetime.strptime(date_new3, "%d %B %Y") # eg. "2021-May-28T00:00:00"
    date_iso8601 = str(date_iso.isoformat())
    '''
except:
    date_iso8601 = ""
    pass

# article body
try:
    article_body = doc.find("div", class_="body").find("div", class_="body-description").find_all("p")
    text = "".join([p.text for p in article_body])
    pass
except:
    text = ""
    pass

id = shortuuid.uuid()
#path_name = slugify(headline, allow_unicode=True)
dest_path = f"{id}/"
if headline != "404":
    if not os.path.exists(dest_path):
        os.makedirs(dest_path)
    else:
        print("The article with this id already exists.")
        pass
    pass
else:
    print("error 404")

def image_finder1(doc):
    valid_imgs = []
    pictures = doc.find_all("picture")
    for picture in pictures:
        img = picture.find("img")
        img_src = img["src"]
        img_name = img_src.split("/")[-1].split("?")[0]

        try:
            img_data = requests.get(img_src, headers=headers).content
            print(f"Downloading {img_name}  ...")
            # we download the image with path img_src into a dist_path and adding .png extension
            with open(f"{dest_path}{img_name}", "wb") as file:
                file.write(img_data)

            try:
                img_desc = img["alt"]
                if img_desc is None or img_desc == "":
                    fig_cap = doc.find("div", class_="photo-credit")
                    img_desc = fig_cap.string

                image = {"filename": img_name, "description": img_desc}
            except:
                img_desc = None
                image = {"filename": img_name}
                pass

            valid_imgs.append(image)
            pass

        except:
            pass
    return valid_imgs



# image XPath article[1]/div/div/div[2]/div/img
# /html/body/div[5]/div/div[8]/div/div[1]/div[1]/div/article/div/div/div[2]/div/img
def image_finder2(doc):
    valid_imgs = []
    header = doc.find_all("article")[0].find("div").find_all("div", class_="widget__head")
    #print(header)
    # TODO must be reviewed
    pictures = header



    for picture in pictures:
        img = picture.find("img")
        img_src = img["src"]
        img_name = img_src.split("/")[-1].split("?")[0]

        try:
            img_data = requests.get(img_src, headers=headers).content

            # removing "." from the filename except the last one
            img_name = img_name.replace(".", "", img_name.count(".") - 1)

            print(f"Downloading {img_name}  ...")
            # we download the image with path img_src into a dist_path and adding .png extension
            with open(f"{dest_path}{img_name}", "wb") as file:
                file.write(img_data)

            try:
                img_desc = img["alt"]
                if img_desc is None or img_desc == "":

                    fig_cap = img.find("figcaption").find("div").find_all("div")[1].find("span").find("span").find("span")
                    img_desc = fig_cap.string

                image = {"filename": img_name, "description": img_desc}
            except:
                img_desc = None
                image = {"filename": img_name}
                pass

            valid_imgs.append(image)

            pass

        except:
            pass
    return valid_imgs


# XPath: article/div/div/div[2]/picture/img
def image_finder3(doc):
    valid_imgs = []
    pictures = doc.find_all("article")[0].find("div").find("div").find_all("div")[1].find("picture").find("img")
    for picture in pictures:
        img = picture.find("img")
        img_src = img["src"]
        img_name = img_src.split("/")[-1].split("?")[0]

        try:
            img_data = requests.get(img_src, headers=headers).content

            # removing "." from the filename except the last one
            img_name = img_name.replace(".", "", img_name.count(".") - 1)

            print(f"Downloading {img_name}  ...")
            # we download the image with path img_src into a dist_path and adding .png extension
            with open(f"{dest_path}{img_name}", "wb") as file:
                file.write(img_data)

            try:
                img_desc = img["alt"]
                if img_desc is None or img_desc == "":

                    fig_cap = img.find("figcaption").find("div").find_all("div")[1].find("span").find("span").find("span")
                    img_desc = fig_cap.string

                image = {"filename": img_name, "description": img_desc}
            except:
                img_desc = None
                image = {"filename": img_name}
                pass

            valid_imgs.append(image)

            pass

        except:
            pass
    return valid_imgs


try:
    valid_imgs = image_finder1(doc)
    try:
        if len(valid_imgs) == 0:
            valid_imgs = image_finder2(doc)
    except:
        if len(valid_imgs) == 0:
            valid_imgs = image_finder3(doc)
except:
    valid_imgs = []
    pass


# tags
# this is a psedu-code. AlterNet does not provide tags
try:
    tags = doc.find("div", class_="article-tags").find_all("a")
    if len(tags) == 1:
        tags = [tags[0].string]
        print(f'only tag: {tags}')
        pass
    else:
        print('multiple tags found')
        tags = [t.string for t in tags]
    pass
except:
    print('no tags found')
    tags = ""
    pass

# update x.4
# if there is no image, delete the folder and stop the program
# execute this part with any cost
finally:
    if not len(valid_imgs) != 0:
        print(f"There are no images in this article. Deleting folder {dest_path} ...")
        shutil.rmtree(dest_path)
        exit()

JSON_dict = {
    "url": path,
    "heading": headline,
    "subheading": subheading,
    "text": text,
    "author": author,
    "publish_date": date_iso8601,
    "tags": tags,
    "img_desc": valid_imgs,
    "id": id
}

with open(f"{dest_path}article.json", "w") as outfile:
    json.dump(JSON_dict, outfile, indent=4)

