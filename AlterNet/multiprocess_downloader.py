# downloading articles using scraper.py and clean_links.txt file in cache folder
# applying multiprocessing

import multiprocessing
import sys
import numpy as np
import requests
import os
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
import time
from joblib import Parallel, delayed
from functools import partial
from random import randrange
import os
import time


with open("cache/clean_links.txt", "r") as file:
    article_links = file.readlines()
    pass

print(f"{len(article_links)} possible articles to download.")

# update x.8
if not os.path.exists("cache/archive.txt"):
    with open("cache/archive.txt", "w") as file:
        file.write("")
with open("cache/archive.txt", "r") as file:
    indx = file.readlines()


for link in article_links:
    if link in indx:
        print(f"link {link} is already downloaded.")
        article_links.remove(link)
         # end of update x.8


def scraper(links_array):
    for link in links_array:
        print(f"link {link}")
        os.system(f"python scraper.py -l {link}")
        print(f"link {link} is checked.")

    return links_array


# Multiprocessing
num_cores = multiprocessing.cpu_count()
batch_size = len(article_links) // 10  # 10% of the links
batches = np.array_split(article_links, batch_size)
scraper_ = partial(scraper)
outputs = Parallel(n_jobs=num_cores, prefer="threads")(delayed(scraper_)(i) for i in batches)
outputs = np.concatenate(outputs)
outputs = np.unique(outputs)
with open("cache/archive.txt", "a") as file:
    for link in outputs:
        file.write(link)

print(f"number of downloaded articles: {len(outputs)}")

